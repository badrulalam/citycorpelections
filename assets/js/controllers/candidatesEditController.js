angular.module('app')
.controller('candidatesEditController',function($scope, $stateParams, $http,$state){
var news = $stateParams.slug;
	$http.get('/candidate/'+news).success(function(response){
		$scope.Candidate = response;
		if(!$scope.Candidate.media_coverage)
		{
			$scope.Candidate.media_coverage = [];
		}
		if(!$scope.Candidate.campaign_links)
		{
			$scope.Candidate.campaign_links = [];
		}

	});
	$scope.editCandidate = function()
	{

		$http.put('/candidate/update/'+$scope.Candidate.id + '?', $scope.Candidate)
			.success(function(data, status, headers, config){
	      $state.go('admin.candidate');


			});
	};
	$scope.addMediaCoverageItem = function(item) {
		$scope.Candidate.media_coverage.push(item);
		$scope.item = {};
	};
	$scope.removeMediaCoverageItem = function(index){
		$scope.Candidate.media_coverage.splice(index,1);
	};
	$scope.addCampaignLinksItem = function(item) {
		$scope.Candidate.campaign_links.push(item);
		$scope.campaign_link = {};
	};
	$scope.removeCampaignLinksItem = function(index){
		$scope.Candidate.campaign_links.splice(index,1);
	}
});
