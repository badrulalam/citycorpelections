angular.module('app')
  .controller('CreateCandidateController', ['$http', '$scope', '$upload','$location', function ($http, $scope, $upload,$location) {
    //$scope.candidates = ['limon','masum','klsd','sfd'];
    $http.get("/candidate")
        .success(function (data) {
            $scope.candidates = data;
        });
	$scope.$watch('files', function () {
		$scope.upload($scope.files);
	});
	$scope.upload = function (files) {
      if (files && files.length) {
        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          console.log("take file");
          console.log($upload);
          $upload.upload({
            url: '/file/upload',
            file: file
          }).progress(function (evt) {
          //  $("#profile-img-cont").empty().html('<i class="fa fa-refresh fa-spin"></i>');
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' +
            evt.config.file.name);
          }).success(function (data, status, headers, config) {
            console.log('filename: ' + config.file.name + ' uploaded. Response: ' +
            JSON.stringify(data));
            $scope.Candidate.image = data.file;
          });
        }
      }
    };
	$scope.addCandidate = function()
	{
		//$scope.Candidate.image = $scope.fileLoc;
		console.log($scope.Candidate);

		$http.post('/candidate/create?',$scope.Candidate)
			.success(function(data, status, headers, config){
				alert("Successfully Candidate Add!");
				$scope.Candidate = {};

			});
	};

  }]);
