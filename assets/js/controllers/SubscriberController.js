angular.module('app')
  .controller('SubscriberController', function($scope, $state, $http) {
      
 
   $http.get("/subscriber")
        .success(function (data) {
            $scope.subscriberList = data;
        });
        

  $scope.deleteSubscriber = function(subscriber)
  {
    console.log(subscriber);
    var result = confirm("Want to delete?");
    if (result) {
        $http.post('/subscriber/destroy/'+subscriber.id)
        .success(function(data, status, headers, config){
          $http.get("/subscriber")
        .success(function (data) {
            $scope.subscriberList = data;
        });
        });
      
    }
  };

  });