angular.module('app')
        .controller('cccvotermapController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {

            $scope.gender_gap_open = true;
            $scope.new_voter_open = false;





          var map_path = "/json/cccvotermap.json";



            function countryClick(featureSelected, leafletEvent)
            {



                var layer = leafletEvent.target;
                layer.setStyle({
                    weight: 2,
                    color: '#666',
                    fillColor: 'black'
                });
                var popupContent = getPCONT(layer, featureSelected);
                layer.bindPopup(popupContent).openPopup();

            }




            var MouseOverStyle =
                    {
                        fillColor: 'white',
                        weight: 3,
                    };



            $scope.showme = false;
            var polygons = [];
            var all_features = [];
            var cl =false;
            var cl_color;



            function collectMe(feature, layer)
            {

                var defstyle;

                polygons[feature.properties.id_3] = layer;
                all_features[feature.properties.id_3] = feature;

                layer.on("mouseout", function (e) {
                    $scope.showme = false;

                    var MouseOutStyle = {
                        weight: 1,
                        fillColor: getColor(feature.properties.m_f_diff)
                    };

                    layer.setStyle(MouseOutStyle);

                });

                layer.on("mouseover", function (e) {
                    $scope.showme = true;

                    $scope.name = feature.properties.name;
                    $scope.gap = feature.properties.m_f_diff;
                    $scope.feminc = feature.properties.female_inc;
                    $scope.femper = feature.properties.female_per;
                    $scope.maleinc = feature.properties.male_inc_n;
                    $scope.maleper = feature.properties.male_per;


                    layer.setStyle(MouseOverStyle);

                });
                layer.on("click", function (e) {
                    $scope.showme = false;
                    var popupContent = getPCONT(feature);
                    layer.bindPopup(popupContent).openPopup();

                });

            }



            function toMap() {
                var duration = 700; //milliseconds
                var offset = 30;
                var map = angular.element(document.getElementById('map'));
                $document.scrollToElementAnimated(map, offset, duration);


            }

            $scope.selectMe = function (id) {

                toMap();

                if(cl){
                 var MouseOutStyle = {
                        weight: 1,
                        fillColor: cl_color
                    };

                    cl.setStyle(MouseOutStyle);
                }

                $location.hash('map');

                //  $anchorScroll();

                var l = polygons[id];
                    l.setStyle({
                      fillColor: 'black'

                   });
                   cl = l;
                   cl_color = getColor(all_features[id].properties.m_f_diff);

                var popupContent = getPCONT(all_features[id]);
                l.bindPopup(popupContent).openPopup();

            };



            function getPCONT(featureSelected)
            {

                var cont = "<table class='table'><tr><td>District</td><td>" + featureSelected.properties.name + "</td></tr><tr><td>Existing Voters</td><td>" + featureSelected.properties.totla_vote + "</td></tr><tr><td>New Female</td><td>" + featureSelected.properties.female_inc + "</td></tr><tr><td>New Female(%)</td><td>" + featureSelected.properties.female_per + "</td></tr><tr><td>New Male</td><td>" + featureSelected.properties.male_inc_n + "</td></tr><tr><td>New Male(%)</td><td>" + featureSelected.properties.male_per + "</td></tr><tr><td>Total New Voters</td><td>" + featureSelected.properties.totla_inc_ + "</td></tr><tr></table>";
                return cont;


            }

            angular.extend($scope, {
                bd: {
                 lat: 22.2207,
                    lng: 91.7500,
                    zoom: 12
                },
                defaults: {
                    scrollWheelZoom: true,
                    attributionControl: false
                },
                layers: {
                    baselayers: {
                        img: {
                            name: 'img',

                             url: '/images/be.png',

                            type: 'xyz'
                        },
                        act: {
                            name: 'Acetate',
                            url: 'http://a{s}.acetate.geoiq.com/tiles/terrain/{z}/{x}/{y}.png',
                            type: 'xyz'
                        },
                    },
                    overlays: {
                        wms: {
                            name: 'Bangladesh',
                            type: 'wms',
                            visible: true,
                            url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                            layerParams: {
                                service: 'WMS',
                                version: '1.1.0',
                                request: 'GetMap',
                                srs: 'EPSG:4326',
                                style: 'polygon',
                                layers: 'distr-map-voter-update-data',
                                format: 'image/png',
                                transparent: true
                            }
                        }
                    }
                }



            });



            function countryMouseover(feature, leafletEvent) {
                var layer = leafletEvent.target;
                layer.setStyle({
                    fillColor: 'white'
                });
                layer.bringToFront();
                $scope.name = feature.properties.name;
                $scope.gap = feature.properties.m_f_diff;

                $scope.femper = feature.properties.female_per;

                $scope.maleper = feature.properties.male_per;


                console.log(feature);
            }


            function getColor(code) {

                var nc = Math.floor(code);
                if (nc < 5)
                {
                    //return "#FE8484";
                    return "#666";
                }

                if (nc >= 5 && nc < 10)
                {
                    //return "#FE5C5C";
                    return "#666";
                }

                if (nc >= 10 && nc < 15)
                {
                    //return "#FE3939";
                    return "#666";
                }


                if (nc >= 15 && nc < 20)
                {
                    //return "#FE2020";
                    return "#666";
                }

                if (nc >= 20 && nc < 25)
                {
                    //return "#DB0000";
                    return "#666";
                }

                if (nc >= 25 && nc < 30)
                {
                    //return "#BA0000";
                    return "#666";
                }

                if (nc >= 30 && nc < 35)
                {
                    //return "#6B0000";
                    return "#666";
                }

                if (nc >= 35 && nc < 40)
                {
                    //return "#530000";
                    return "#666";
                }

                if (nc >= 40)
                {
                    //return "#250000";
                    return "#666";
                }
            }

            function resetHighlight(e) {
                geojson.resetStyle(e.target);
            }
            function style(feature)
            {
                return {
                    fillColor: getColor(feature.properties.m_f_diff),
                    weight: 1,
                    opacity: 1,
                    color: 'white',
                    //  dashArray: '3',
                    fillOpacity: 0.7
                };
            }


            $http.get(map_path).success(function (data, status) {
                angular.extend($scope, {
                    geojson: {
                        data: data,
                        style: style,
                        mouseout: resetHighlight,
                        resetStyleOnMouseout: true,
                        onEachFeature: collectMe
                    }
                });
                $scope.features = data.properties;
            });







        })
