angular.module('app')
  .controller('LoginController', function($scope, $state, Auth, CurrentUser, LocalService) {
    $scope.errors = [];
if (LocalService.get('auth_token')) {
     
        $state.go('admin');
    
    }
    $scope.login = function() {
      $scope.errors = [];
      Auth.login($scope.user).success(function(result) {
        $state.go('admin');
      }).error(function(err) {
        $scope.errors.push(err);
      });
    }
  });