angular.module('app')
  .controller('candidatesController', function($scope, $state, $http, $location, $anchorScroll) {
    //$scope.candidates = ['limon','masum','klsd','sfd'];

    $scope.totalCandidates = 0;
    $http.get("candidate/getTotalCandidates").success(function(data){
      $scope.totalCandidates  = data;
    });

    $http.get("candidate/getCandidatesByLimit?limit=50&order=desc").success(function(data) {
      //console.log("success hoise");
    //  console.log(data);
    //});
    //
    //$http.get("/candidate")
    //  .success(function(data) {
        $scope.candidateList = data;
      });


      //init candidate for create
      $scope.Candidate = {};
      $scope.Candidate.media_coverage = [];
      $scope.Candidate.campaign_links = [];
    $http.get("/candidate?city=ccc&type=mayor")
      .success(function(data) {
        $scope.ccc_candidates = data;
      });

    $http.get("/candidate?city=dncc&type=mayor")
      .success(function(data) {
        $scope.dncc_candidates = data;
      });

    $http.get("/candidate?city=dscc&type=mayor")
      .success(function(data) {
        $scope.dscc_candidates = data;
      });
    angular.element(document).ready(function() {




      $("#owl-example").owlCarousel({
        autoPlay: false, //Set AutoPlay to 3 seconds
        pagination: false,
        navigation: true,
        navigationText: [
          "<i class='icon-chevron-left icon-white'></i>",
          "<i class='icon-chevron-right icon-white'></i>"
        ],
        items: 6,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3]
      });



    });
    $scope.changeTab = function(id) {
      document.getElementById('chittagong').className = "tab-pane";
      document.getElementById('north').className = "tab-pane";
      document.getElementById('south').className = "tab-pane";
      if (id == "chittagong") {
        document.getElementById('tab_button_ctg').className = "tabheader active";
        document.getElementById('tab_button_north').className = "tabheader ";
        document.getElementById('tab_button_south').className = "tabheader ";
      }
      if (id == "north") {
        document.getElementById('tab_button_ctg').className = "tabheader";
        document.getElementById('tab_button_north').className = "tabheader active";
        document.getElementById('tab_button_south').className = "tabheader ";
      }
      if (id == "south") {

        document.getElementById('tab_button_ctg').className = "tabheader";
        document.getElementById('tab_button_north').className = "tabheader";
        document.getElementById('tab_button_south').className = "tabheader active";
      }
      var NAME = document.getElementById(id);
      NAME.className = "tab-pane active";
    };
    $scope.owlOptions = {
      autoPlay: false, //Set AutoPlay to 3 seconds
      pagination: false,
      navigation: true,
      navigationText: [
        "<i class='icon-chevron-left icon-white'><</i>",
        "<i class='icon-chevron-right icon-white'>></i>"
      ],
      items: 6,
      itemsDesktop: [1199, 3],
      itemsDesktopSmall: [979, 3]
    };
    $scope.addCandidate = function() {

      //$scope.Candidate.image = $scope.fileLoc;
      console.log($scope.Candidate);

      $http.post('/candidate/create?', $scope.Candidate)
        .success(function(data, status, headers, config) {
          alert("Successfully Candidate Add!");
          $scope.Candidate = {};
          $scope.Candidate.media_coverage = [];
          $scope.Candidate.campaign_links = [];
        });
    };
    $scope.showCandidate = function(id) {
      alert(id);
    };
    $scope.deleteCandidate = function(malp) {
      console.log(malp);
      var result = confirm("Want to delete?");
      if (result) {
        $http.post('/candidate/destroy/' + malp.id)
          .success(function(data, status, headers, config) {
            // $http.get("/candidate")
            //   .success(function(data) {
            //     $scope.candidateList = data;
            //   });
          });

      }
    };
    $scope.addMediaCoverageItem = function(item) {
			$scope.Candidate.media_coverage.push(item);
			$scope.item = {};
		};
		$scope.removeMediaCoverageItem = function(index){
      $scope.Candidate.media_coverage.splice(index,1);
		};
    $scope.addCampaignLinksItem = function(item) {
      $scope.Candidate.campaign_links.push(item);
      $scope.campaign_link = {};
    };
    $scope.removeCampaignLinksItem = function(index){
      $scope.Candidate.campaign_links.splice(index,1);
    };

      $scope.gotoBottom = function() {
      // set the location.hash to the id of
      // the element you wish to scroll to.
      $location.hash('mcandi_cont');

      // call $anchorScroll()
      $anchorScroll();
    };

  });
