angular.module('app')
  .controller('MalpracticeController', ['$http', '$scope', '$upload','$location', function ($http, $scope, $upload,$location) {
   $http.get("/malpractice")
        .success(function (data) {
            $scope.malpracticeList = data;
        });
   $scope.$watch('files', function () {
		$scope.upload($scope.files);
	});
	$scope.upload = function (files) {
      if (files && files.length) {
        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          console.log("take file");
          console.log($upload);
          $upload.upload({
            url: '/file/upload',
            file: file
          }).progress(function (evt) {
          //  $("#profile-img-cont").empty().html('<i class="fa fa-refresh fa-spin"></i>');
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' +
            evt.config.file.name);
          }).success(function (data, status, headers, config) {
            console.log('filename: ' + config.file.name + ' uploaded. Response: ' +
            JSON.stringify(data));
            var filepath =  data.file.replace("/var/www/citycorpelections/", "");
            $scope.fileImage = filepath;
          });
        }
      }
    };
	$scope.addMalpractice = function()
	{
		$scope.Malpractice.image = $scope.fileImage;
		console.log($scope.Malpractice);
		$http.post('/malpractice/create?',$scope.Malpractice)
			.success(function(data, status, headers, config){
				alert("Malpractice Submit success!");
				$scope.Malpractice = {};
        $scope.filepath ="";
			});
	};
  $scope.deleteMalpractice = function(malp)
  {
    console.log(malp);
    var result = confirm("Want to delete?");
    if (result) {
        $http.post('/malpractice/destroy/'+malp.id)
        .success(function(data, status, headers, config){
          $http.get("/malpractice")
        .success(function (data) {
            $scope.malpracticeList = data;
        });
        });
      
    }
  };
  }]);