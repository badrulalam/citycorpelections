angular.module('app')
  .controller('candidatesMayoralController', function($scope,$stateParams, $state, $http) {
    //$scope.candidates = ['limon','masum','klsd','sfd'];
var city = $stateParams.slug;
    $scope.totalCandidates = 0;
    $scope.totalDNCC = 0;
    $scope.totalDSCC = 0;
    $scope.totalCCC = 0;
    $http.get("candidate/getTotalCandidates").success(function(data){
      $scope.totalCandidates  = data;
    });

    $http.get("candidate/getTotalTypeCandidates?city=dncc").success(function(data){
      $scope.totalDNCC  = data;
    });

    $http.get("candidate/getTotalTypeCandidates?city=dscc").success(function(data){
      $scope.totalDSCC  = data;
    });

    $http.get("candidate/getTotalTypeCandidates?city=ccc").success(function(data){
      $scope.totalCCC  = data;
    });


    //$http.get("candidate/getCandidatesByLimit?limit=50&order=desc").success(function(data) {
      //console.log("success hoise");
    //  console.log(data);
    //});
    //
    $http.get("/candidate?type=mayor&city="+city)
      .success(function(data) {
        $scope.candidateList = data;
      });


    //$http.get("/candidate?city=ccc&type=mayor")
    //  .success(function(data) {
    //    $scope.ccc_candidates = data;
    //  });
    //
    //$http.get("/candidate?city=dncc&type=mayor")
    //  .success(function(data) {
    //    $scope.dncc_candidates = data;
    //  });
    //
    //$http.get("/candidate?city=dscc&type=mayor")
    //  .success(function(data) {
    //    $scope.dscc_candidates = data;
    //  });

    $scope.addCandidate = function() {

      //$scope.Candidate.image = $scope.fileLoc;
      console.log($scope.Candidate);

      $http.post('/candidate/create?', $scope.Candidate)
        .success(function(data, status, headers, config) {
          alert("Successfully Candidate Add!");
          $scope.Candidate = {};

        });
    };
    $scope.showCandidate = function(id) {
      alert(id);
    };
    $scope.deleteCandidate = function(malp) {
      console.log(malp);
      var result = confirm("Want to delete?");
      if (result) {
        $http.post('/candidate/destroy/' + malp.id)
          .success(function(data, status, headers, config) {
            $http.get("/candidate")
              .success(function(data) {
                $scope.candidateList = data;
              });
          });

      }
    };

  });
