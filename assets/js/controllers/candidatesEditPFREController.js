angular.module('app')
.controller('candidatesEditPFREController',function($scope, $stateParams, $http,$state){
var news = $stateParams.slug;
    //console.log("candidate detail");
    //console.log(news);
	$http.get('/candidate/'+news).success(function(response){
		$scope.Candidate = response;
    //console.log(response);
	});
	$scope.editCandidate = function()
	{

		$http.put('/candidate/update/'+$scope.Candidate.id + '?', $scope.Candidate)
			.success(function(data, status, headers, config){
	      $state.go('admin.candidate');


			});
	};
});
