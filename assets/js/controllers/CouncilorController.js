angular.module('app')
  .controller('CouncilorController', function($scope, $state, $http, $location) {




    $http.get("/candidate?type=council")
        .success(function (data) {
            $scope.candidateList = data;

      });

    //     $http.get("/candidate?city=ccc&type=council")
    //          .success(function (data) {
    //            $scope.ccc_candidates =data;
    //          });
     //
    //  $http.get("/candidate?city=dncc&type=council")
    //    .success(function (data) {
    //      $scope.dncc_candidates = data;
    //    });
     //
    //  $http.get("/candidate?city=dscc&type=council")
    //    .success(function (data) {
    //      $scope.dscc_candidates = data;
    //    });
       $http.get("/json/dnccward.json")
         .success(function (data) {
           $scope.dnccward = data;
         });
         $http.get("/json/dsccward.json")
           .success(function (data) {
             $scope.dsccward = data;
           });
           $http.get("/json/cccward.json")
             .success(function (data) {
               $scope.cccward = data;
             });



       $scope.changeTab = function(id) {
         document.getElementById('chittagong').className = "tab-pane";
         document.getElementById('north').className = "tab-pane";
         document.getElementById('south').className = "tab-pane";
         if(id=="chittagong")
         {
           document.getElementById('tab_button_ctg').className = "tabheader active";
           document.getElementById('tab_button_north').className = "tabheader ";
           document.getElementById('tab_button_south').className = "tabheader ";
         }
         if(id=="north")
         {
           document.getElementById('tab_button_ctg').className = "tabheader";
           document.getElementById('tab_button_north').className = "tabheader active";
           document.getElementById('tab_button_south').className = "tabheader ";
         }
         if(id=="south")
         {

           document.getElementById('tab_button_ctg').className = "tabheader";
           document.getElementById('tab_button_north').className = "tabheader";
           document.getElementById('tab_button_south').className = "tabheader active";
         }
         var NAME = document.getElementById(id);
         NAME.className = "tab-pane active";
         $("#councilor-data").css("visibility","hidden");
         $("#councilor-list").css("visibility","hidden");
         $scope.candidates = "";
         $scope.ward_title = "";
         $state.go('councilor');
       };
       $scope.selectWard = function(id, city, name)
       {
         $("#councilor-data").css("visibility","visible");
           $("#councilor-list").css("visibility","visible");
         $http.get("/candidate?city="+ city+"&type=council&ward="+id)
           .success(function (data) {
             $scope.candidates = data;
             $scope.ward_title = name;


      angular.element(document).ready(function(){
                 $('html, body').animate({
      scrollTop:$("#councilor-list").offset().top
      }, 1000);});


      $state.go('councilor');


           });
       };

});
