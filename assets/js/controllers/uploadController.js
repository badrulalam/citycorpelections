angular.module('app')
  .controller('UploadController', ['$http', '$scope', '$upload','$location', function ($http, $scope, $upload,$location) {

    $scope.fileLoc = "images/demo-profile-pic.png";
    $scope.loggedinUser = {};

    //var currentUser = CurrentUser.user();

    // $http.post('/user/find?id='+currentUser.id).success(function (data, status, headers, config) {
    //   $scope.loggedinUser = data;
    //   console.log($scope.loggedinUser);

    //   if($scope.loggedinUser.myprofile_image!=='')
    //     $scope.fileLoc = $scope.loggedinUser.myprofile_image;

    //   //console.log($scope.loggedinUser);
    //   $scope.profilePics = [];
    //   if($scope.loggedinUser.profile_images.length)
    //     $scope.profilePics = $scope.loggedinUser.profile_images;

    // }).error(function (data, status, headers, config) {
    //   // called asynchronously if an error occurs
    //   // or server returns response with an error status.
    // });

    //$http.post('/profileimages/find?user_id='+currentUser.id).success(function (data, status, headers, config) {
    //  console.log("successfull");
    //  console.log(data);
    //  $scope.profilePics = data;
    //}).error(function (data, status, headers, config) {
    //  // called asynchronously if an error occurs
    //  // or server returns response with an error status.
    //});

    //file uploading
    $scope.$watch('files', function () {
      $scope.upload($scope.files);
    });

    $scope.upload = function (files) {
      if (files && files.length) {
        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          console.log("take file");
          console.log($upload);
          $upload.upload({
            url: '/file/upload',
            file: file
          }).progress(function (evt) {
          //  $("#profile-img-cont").empty().html('<i class="fa fa-refresh fa-spin"></i>');
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' +
            evt.config.file.name);
          }).success(function (data, status, headers, config) {
            console.log('filename: ' + config.file.name + ' uploaded. Response: ' +
            JSON.stringify(data));
            $scope.fileLoc = data.file;
          });
        }
      }
    };

    //changing profile pic
    $scope.chooseThis = function(imgId){
      console.log(imgId);
    }

    $scope.deleteThis = function(imgId){
      console.log(imgId);
      $http.post('/profileimages/destroy?id='+imgId).success(function (data, status, headers, config) {
        console.log("successfull");
        console.log(data);
      }).error(function (data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }

}]);
