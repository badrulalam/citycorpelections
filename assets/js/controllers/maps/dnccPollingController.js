
angular.module('app')
        .controller('dnccPollingController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {
          var map_path = "/json/dncc_polling_stations_2015_new.geojson";

          var ward_label = "/json/ward-label-dncc.json";

  //         var wardList = [];
  //         var uniqueNames = [];
  //                     $http.get(map_path).success(function (data, status) {
  //                       for (index = 0; index < data.features.length; ++index) {
  //                         wardList[index] = data.features[index].properties.ward;
  //                       }
  //
  //                       $.each(wardList, function(i, el){
  //     if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
  // });
  // $scope.wardList = uniqueNames;
  //                     //  console.log(uniqueNames);
  //
  //
  //
  //                     });








                 angular.extend($scope, {
                bd: {
                    lat: 23.7088,
                    lng: 90.40,
                    zoom: 13
                },
                defaults: {
                    scrollWheelZoom: true,
                    attributionControl: false
                },
                layers: {
                    baselayers: {
                        osm: {
                            name: 'osm',
                            url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                            type: 'xyz'
                        },

                    },

               overlays: {
                wms: {
                    name: 'CCC',
                    type: 'wms',
                    visible: true,
                    url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                    layerParams: {
                        service: 'WMS',
                        version: '1.1.0',
                        request: 'GetMap',
                        srs: 'EPSG:4326',
                        style:'whitepol',
                        layers: 'dncc-voter-population',
                        format: 'image/png',
                        transparent: true
                    }
                }
            }


                }



            });



            function countryClick(featureSelected, leafletEvent) {



                var layer = leafletEvent.target;

                var popupContent = getPCONT(layer, featureSelected);
                layer.bindPopup(popupContent).openPopup();

            }





            $scope.showme = false;
            var polygons = [];
            var all_features = [];
            var cl = false;
            var cl_color;

            function collectMe(feature, layer) {


              layer.on("click", function (e) {
                  $scope.showme = true;

                  var popupContent = getPCONT(feature);
                  console.log(feature.properties);
                  var totalcenter = feature.properties.totalcenter;
                  $scope.name = feature.properties.name;
                  var pollingdata = [];
                  for(var i=1;i<= totalcenter; i++)
                  {
                      if(i==1){
                      var polling = {};
                      polling.center = "Center 1";
                      polling.voter = feature.properties.voter_1;
                      polling.booth = feature.properties.booth_1;
                      polling.sl = feature.properties.sl_1;
                      polling.type = feature.properties.type_1;
                      pollingdata[i-1] = polling;
                      }
                      if(i==2){
                      var polling = {};
                      polling.center = "Center 2";
                      polling.voter = feature.properties.voter_2;
                      polling.booth = feature.properties.booth_2;
                      polling.sl = feature.properties.sl_2;
                      polling.type = feature.properties.type_2;
                      pollingdata[i-1] = polling;
                      }
                      if(i==3){
                      var polling = {};
                      polling.center = "Center 3";
                      polling.voter = feature.properties.voter_3;
                      polling.booth = feature.properties.booth_3;
                      polling.sl = feature.properties.sl_3;
                      polling.type = feature.properties.type_3;
                      pollingdata[i-1] = polling;
                      }
                      if(i==4){
                      var polling = {};
                      polling.center = "Center 4";
                      polling.voter = feature.properties.voter_4;
                      polling.booth = feature.properties.booth_4;
                      polling.sl = feature.properties.sl_4;
                      polling.type = feature.properties.type_4;
                      pollingdata[i-1] = polling;
                      }
                      if(i==5){
                      var polling = {};
                      polling.center = "Center 5";
                      polling.voter = feature.properties.voter_5;
                      polling.booth = feature.properties.booth_5;
                      polling.sl = feature.properties.sl_5;
                      polling.type = feature.properties.type_5;
                      pollingdata[i-1] = polling;
                      }
                      if(i==6){
                      var polling = {};
                      polling.center = "Center 6";
                      polling.voter = feature.properties.voter_6;
                      polling.booth = feature.properties.booth_6;
                      polling.sl = feature.properties.sl_6;
                      polling.type = feature.properties.type_6;
                      pollingdata[i-1] = polling;
                      }
                      if(i==7){
                      var polling = {};
                      polling.center = "Center 7";
                      polling.voter = feature.properties.voter_7;
                      polling.booth = feature.properties.booth_7;
                      polling.sl = feature.properties.sl_7;
                      polling.type = feature.properties.type_7;
                      pollingdata[i-1] = polling;
                      }
                      if(i==8){
                      var polling = {};
                      polling.center = "Center 8";
                      polling.voter = feature.properties.voter_8;
                      polling.booth = feature.properties.booth_8;
                      polling.sl = feature.properties.sl_8;
                      polling.type = feature.properties.type_8;
                      pollingdata[i-1] = polling;
                      }
                      if(i==9){
                      var polling = {};
                      polling.center = "Center 9";
                      polling.voter = feature.properties.voter_9;
                      polling.booth = feature.properties.booth_9;
                      polling.sl = feature.properties.sl_9;
                      polling.type = feature.properties.type_9;
                      pollingdata[i-1] = polling;
                      }
                      if(i==10){
                      var polling = {};
                      polling.center = "Center 10";
                      polling.voter = feature.properties.voter_10;
                      polling.booth = feature.properties.booth_10;
                      polling.sl = feature.properties.sl_10;
                      polling.type = feature.properties.type_10;
                      pollingdata[i-1] = polling;
                      }

                  }
                  $scope.pollingdata = pollingdata;
                  $scope.image = feature.properties.image;
                //  console.log(pollingdata);

              });

            }







            function getPCONT(featureSelected) {

                var cont = "<table class='table'><tr><td>Name</td><td>" + featureSelected.properties.male + "</td></tr></table>";
                return cont;


            }


            function plotLayer(feature, latlng){
        return L.circleMarker(latlng, {
          radius: 8,
          fillColor: "#ff7800",
          color: "#000",
          weight: 1,
          opacity: 1,
          fillOpacity: 0.8
        });
        };
        var ward = "21";
        function filterLayer(feature, latlng){
          return feature.properties.ward == ward;
        };







// $scope.loadPS = function(w)
// {
//   ward = w;

  $http.get(map_path).success(function (data, status) {
      angular.extend($scope, {
          geojson: {
              data: data,
             selectedCountry: {},
              onEachFeature: collectMe,
              // filter: filterLayer,
              pointToLayer: plotLayer,
          }
      });
      $scope.features = data.properties;
  });
  //alert(ward);
// };

            $http.get(ward_label).success(function (data, status) {
                angular.extend($scope, {
                  markers: data,
                });
            });



        });
