
angular.module('app')
        .controller('cccPollingController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {


              var map_path = "/json/ccc_polling_stations_2015_new.geojson";
              var ward_label = "/json/ward-label-ccc.json";

              var mapOptions = 
                 angular.extend($scope, {
                bd: {
                    lat: 22.233059,
                    lng: 91.835247,
                    zoom: 12
                },
                defaults: {
                    scrollWheelZoom: true,
                    attributionControl: false
                },
                layers: {
                    baselayers: {
                        osm: {
                            name: 'osm',
                            url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                            type: 'xyz'
                        },

                    },
                               overlays: {
                wms: {
                    name: 'CCC',
                    type: 'wms',
                    visible: true,
                    url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                    layerParams: {
                        service: 'WMS',
                        version: '1.1.0',
                        request: 'GetMap',
                        srs: 'EPSG:4326',
                        style:'whitepol',
                        layers: 'ccc-ward-voters',
                        format: 'image/png',
                        transparent: true
                    }
                }
            }
                }
            });



            function countryClick(featureSelected, leafletEvent) {



                var layer = leafletEvent.target;

                var popupContent = getPCONT(layer, featureSelected);
                layer.bindPopup(popupContent).openPopup();

            }





            $scope.showme = false;
            var polygons = [];
            var all_features = [];
            var cl = false;
            var cl_color;

            function collectMe(feature, layer) {

              layer.on("click", function (e) {
                  $scope.showme = true;

                  var popupContent = getPCONT(feature);
                  //console.log(feature.properties);
                  var totalcenter = feature.properties.totalcenter;
                  $scope.name = feature.properties.name;
                  var pollingdata = [];
                  for(var i=1;i<= totalcenter; i++)
                  {
                      if(i==1){
                      var polling = {};
                      polling.center = "Center 1";
                      polling.voter = feature.properties.voter_1;
                      polling.booth = feature.properties.booth_1;
                      polling.sl = feature.properties.sl_1;
                      polling.type = feature.properties.type_1;
                      pollingdata[i-1] = polling;
                      }
                      if(i==2){
                      var polling = {};
                      polling.center = "Center 2";
                      polling.voter = feature.properties.voter_2;
                      polling.booth = feature.properties.booth_2;
                      polling.sl = feature.properties.sl_2;
                      polling.type = feature.properties.type_2;
                      pollingdata[i-1] = polling;
                      }
                      if(i==3){
                      var polling = {};
                      polling.center = "Center 3";
                      polling.voter = feature.properties.voter_3;
                      polling.booth = feature.properties.booth_3;
                      polling.sl = feature.properties.sl_3;
                      polling.type = feature.properties.type_3;
                      pollingdata[i-1] = polling;
                      }
                      if(i==4){
                      var polling = {};
                      polling.center = "Center 4";
                      polling.voter = feature.properties.voter_4;
                      polling.booth = feature.properties.booth_4;
                      polling.sl = feature.properties.sl_4;
                      polling.type = feature.properties.type_4;
                      pollingdata[i-1] = polling;
                      }
                      if(i==5){
                      var polling = {};
                      polling.center = "Center 5";
                      polling.voter = feature.properties.voter_5;
                      polling.booth = feature.properties.booth_5;
                      polling.sl = feature.properties.sl_5;
                      polling.type = feature.properties.type_5;
                      pollingdata[i-1] = polling;
                      }
                      if(i==6){
                      var polling = {};
                      polling.center = "Center 6";
                      polling.voter = feature.properties.voter_6;
                      polling.booth = feature.properties.booth_6;
                      polling.sl = feature.properties.sl_6;
                      polling.type = feature.properties.type_6;
                      pollingdata[i-1] = polling;
                      }

                  }
                  $scope.pollingdata = pollingdata;
                  $scope.image = feature.properties.Image;
                //  console.log(pollingdata);

              });
            }





            $scope.selectMe = function (id) {

                toMap();

                if (cl) {
                    var MouseOutStyle = {
                        weight: 1,
                        fillColor: cl_color
                    };

                    cl.setStyle(MouseOutStyle);
                }
                $location.hash('map');

                //  $anchorScroll();

                var l = polygons[id];
                l.setStyle({
                    fillColor: 'yellow'

                });

                cl = l;
                cl_color = getColor(all_features[id].WARD);

                var popupContent = getPCONT(all_features[id]);
                l.bindPopup(popupContent, {offset: [0, -40]}).openPopup();

            };



            function getPCONT(featureSelected) {

                var cont = "<table class='table'><tr><td>WARD</td><td>" + featureSelected.properties.WARD + "</td></tr></table>";
                return cont;


            }







            $http.get(map_path).success(function (data, status) {
                angular.extend($scope, {
                    geojson: {
                        data: data,
                       selectedCountry: {},
                        onEachFeature: collectMe,
                        pointToLayer: function (feature, latlng) {
				return L.circleMarker(latlng, {
					radius: 8,
					fillColor: "#ff7800",
					color: "#000",
					weight: 1,
					opacity: 1,
					fillOpacity: 0.8
				});
			}
                    }
                });
                $scope.features = data.properties;
            });


                        $http.get(ward_label).success(function (data, status) {
                            angular.extend($scope, {
                              markers: data,
                            });
                        });





        });
