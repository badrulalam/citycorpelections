
angular.module('app')
        .controller('dsccPopulationController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {







              var map_path = "/json/dsccfinal6.geojson";
              var ward_label = "/json/ward-label-dscc.json";


              var myIcon = L.divIcon({className: 'leaflet-div-icon'});
            function countryClick(featureSelected, leafletEvent) {



                var layer = leafletEvent.target;
                layer.setStyle({
                    weight: 2,
                    color: '#666',
                    fillColor: 'black'
                });
                var popupContent = getPCONT(layer, featureSelected);
                layer.bindPopup(popupContent).openPopup();

            }




            var MouseOverStyle = {
                fillColor: '#000'
            };

            $scope.showme = false;
            var polygons = [];
            var all_features = [];
            var cl = false;
            var cl_color;

            function collectMe(feature, layer) {

                var defstyle;

                polygons[feature.properties.NEW_WARD] = layer;
                all_features[feature.properties.NEW_WARD] = feature;

                layer.on("mouseout", function (e) {
                    $scope.showme = false;

                    var MouseOutStyle = {
                        fillColor: getColor(feature.properties.NEW_WARD)
                    };

                    layer.setStyle(MouseOutStyle);

                });

                layer.on("mouseover", function (e) {
                  $scope.showme = true;
                  $scope.ward = feature.properties.NEW_WARD;
                  $scope.population = feature.properties.Popu;
                  $scope.voter = feature.properties.voters;


                    layer.setStyle(MouseOverStyle);

                });
                layer.on("click", function (e) {
                    $scope.showme = false;
                    var popupContent = getPCONT(feature);
                    layer.bindPopup(popupContent, {offset: [0, -20]}).openPopup();

                });

            }



            function toMap() {
                var duration = 700; //milliseconds
                var offset = 30;
                var map = angular.element(document.getElementById('map'));
                $document.scrollToElementAnimated(map, offset, duration);


            }

            $scope.selectMe = function (id) {

                toMap();

                if (cl) {
                    var MouseOutStyle = {
                        weight: 1,
                        fillColor: cl_color
                    };

                    cl.setStyle(MouseOutStyle);
                }
                $location.hash('map');

                //  $anchorScroll();

                var l = polygons[id];
                l.setStyle({
                    fillColor: 'yellow'

                });

                cl = l;
                cl_color = getColor(all_features[id].NEW_WARD);

                var popupContent = getPCONT(all_features[id]);
                l.bindPopup(popupContent, {offset: [0, -40]}).openPopup();

            };



            function getPCONT(featureSelected) {

                var cont = "<table class='table'><tr><td>WARD</td><td>" + featureSelected.properties.NEW_WARD + "</td></tr></table>";
                return cont;


            }

            angular.extend($scope, {
                bd: {
                  lat: 23.6450,
                  lng: 90.42,
                  zoom: 13
                },
                defaults: {
                    scrollWheelZoom: true,
                    attributionControl: false
                },
                layers: {
                    baselayers: {
                        osm: {
                            name: 'osm',
                            url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                            type: 'xyz'
                        },

                    }
                }



            });

            function countryMouseover(feature, leafletEvent) {
                var layer = leafletEvent.target;
                layer.setStyle({
                    weight: 2,
                    color: '#666',
                    fillColor: 'white'
                });
                layer.bringToFront();
                $scope.name = feature.properties.name;
                $scope.gap = feature.properties.m_f_diff;

                $scope.femper = feature.properties.female_per;

                $scope.maleper = feature.properties.male_per;


                console.log(feature);
            }


            function getColor(code) {

                var nc = parseFloat(code);
                if (nc < 5)
                {
                    //return "#78D261";
                    return "#78D261";
                }

                if (nc >= 5 && nc < 10)
                {
                    //return "#0CA300";
                    return "#78D261";
                }

                if (nc >= 10 && nc < 15)
                {
                    //return "#0A8300";
                    return "#78D261";
                }


                if (nc >= 15 && nc < 20)
                {
                    //return "#076600";
                    return "#78D261";
                }

                if (nc >= 20 && nc < 30)
                {
                    //return "#054700";
                    return "#78D261";
                }

                if (nc >= 30 && nc < 62)
                {
                    //return "#053400";
                    return "#78D261";
                }



            }

            function resetHighlight(e) {
                geojson.resetStyle(e.target);
            }
            function style(feature)
            {
                return {
                    fillColor: getColor(feature.properties.NEW_WARD),
                    weight: 1,
                    opacity: 0.7,
                    color: 'white',
                    //  dashArray: '3',
                    fillOpacity: 0.7
                };
            }


            $http.get(map_path).success(function (data, status) {
                angular.extend($scope, {
                    geojson: {
                        data: data,
                        style: style,
                        mouseout: resetHighlight,
                        resetStyleOnMouseout: true
                        , selectedCountry: {},
                        onEachFeature: collectMe,

                    }
                });
                $scope.features = data.properties;
            });

        // var div_icon = [];
        // for(var i=0;i< 100; i++)
        // {
        //   var ico = {};
        //   ico = {
        //     type: 'div',
        //     iconSize: [20, 20],
        //     html: '<b>'+i+'</b>',
        //     popupAnchor:  [0, 0]
        //   };
        //   div_icon[i]=ico;
        // }
        // var markers = {};
        // for(var i=0;i< 56; i++)
        // {
        //   var ico = {};
        //   ico = {
        //     lat: 23.7358,
        //     lng: 90.42,
        //       icon: div_icon[i+1],
        //       focus: false,
        //       //clickable: false,
        //       draggable: true
        //   };
        //   markers[i]=ico;
        // }

        $http.get(ward_label).success(function (data, status) {
            angular.extend($scope, {
              markers: data,
            });
        });

            // $scope.$on('leafletDirectiveMap.drag', function(event){
            //     console.log(JSON.stringify(markers));
            // });
        })
