// angular.module('app')
// .controller('CouncilorStateController',function($scope, $stateParams, $http,$state){
//
// var city = $stateParams.city;
// $scope. city = city;
//
// });
//


angular.module('app')
        .controller('CouncilorStateController', function ($scope, $stateParams, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {


          var city = $stateParams.city;

if(city == "dscc")
{
  var map_path = "/json/dsccfinal6.geojson";
  var ward_label = "/json/ward-label-dscc.json";
  var       bd =  {
            lat: 23.6358,
            lng: 90.42,
            zoom: 13
        };

}
if(city == "dncc")
{
  var map_path = "/json/dnccfinal6.geojson";

  var ward_label = "/json/ward-label-dncc.json";
  var bd =  {
      lat: 23.7528,
      lng: 90.345,
      zoom: 13
  };

}
if(city == "ccc")
{
  var map_path = "/json/ccc-ward-voter-popu.geojson";
  var ward_label = "/json/ward-label-ccc.json";
  var bd =  {
      lat: 22.2207,
      lng: 91.7500,
      zoom: 12
  };

}

angular.extend($scope, {
 bd: bd,
 defaults: {
     scrollWheelZoom: true,
     attributionControl: false
 },
 layers: {
     baselayers: {
         osm: {
             name: 'osm',
             url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
             type: 'xyz'
         },

     }
 }



});

var dsccward = {};
var dnccward = {};
var cccward = {};
$http.get('/json/dsccward.json').success(function (data, status) {
   dsccward = data;
});
$http.get('/json/dnccward.json').success(function (data, status) {
   dnccward = data;
});
$http.get('/json/cccward.json').success(function (data, status) {
   cccward = data;
});

            function countryClick(featureSelected, leafletEvent) {



                var layer = leafletEvent.target;
                layer.setStyle({
                    weight: 2,
                    color: '#666',
                    fillColor: 'black'
                });
                var popupContent = getPCONT(layer, featureSelected);
                layer.bindPopup(popupContent).openPopup();

            }




            var MouseOverStyle = {
                fillColor: '#000'
            };

            $scope.showme = false;
            var polygons = [];
            var all_features = [];
            var cl = false;
            var cl_color;

            function collectMe(feature, layer) {

                var defstyle;

                polygons[feature.properties.WARD] = layer;
                all_features[feature.properties.WARD] = feature;

                // layer.on("mouseout", function (e) {
                //     $scope.showme = false;
                //
                //     var MouseOutStyle = {
                //         fillColor: getColor()
                //     };
                //
                //     layer.setStyle(MouseOutStyle);
                //
                // });

                layer.on("click", function (e) { //console.log(feature.properties);

                  if(city == "dscc")
                  {
                    $scope.ward = feature.properties.NEW_WARD;
                    $scope.population = feature.properties.Popu;
                    for (var i in dsccward) {
                     if(dsccward[i].label == feature.properties.NEW_WARD && dsccward[i].type == 'ward'){
                       $http.get('candidate?type=council&city=dscc&is_winner=true&ward='+dsccward[i].id).success(function (data, status) {
                         $scope.councilorCandidate  = data;
                         });
                     }

                    }

                  }
                  if(city == "dncc")
                  {
                    $scope.ward = feature.properties.NEW_WARD;
                    $scope.population = feature.properties.popu;

                    for (var i in dnccward) {
                     if(dnccward[i].label == feature.properties.NEW_WARD && dnccward[i].type == 'ward'){
                       $http.get('candidate?type=council&city=dncc&is_winner=true&ward='+dnccward[i].id).success(function (data, status) {
                         $scope.councilorCandidate  = data;
                         });
                     }

                    }
                  }
                  if(city == "ccc")
                  {
                    $scope.showme = true;
                    $scope.ward = feature.properties.WARD;
                    $scope.population = feature.properties.population;


                    for (var i in cccward) {
                    if(cccward[i].label == feature.properties.WARD && cccward[i].type == 'ward'){
                      $http.get('candidate?type=council&city=ccc&is_winner=true&ward='+cccward[i].id).success(function (data, status) {
                          $scope.councilorCandidate  = data;
                        });
                    }

                    }



                  }

                    $scope.showme = true;
                    //layer.setStyle(MouseOverStyle);

                });
                // layer.on("click", function (e) {
                //     $scope.showme = false;
                //     var popupContent = getPCONT(feature);
                //     layer.bindPopup(popupContent, {offset: [0, -20]}).openPopup();
                //
                // });

            }



            function toMap() {
                var duration = 700; //milliseconds
                var offset = 30;
                var map = angular.element(document.getElementById('map'));
                $document.scrollToElementAnimated(map, offset, duration);


            }

            $scope.selectMe = function (id) {

                toMap();

                if (cl) {
                    var MouseOutStyle = {
                        weight: 1,
                        fillColor: cl_color
                    };

                    cl.setStyle(MouseOutStyle);
                }
                $location.hash('map');

                //  $anchorScroll();

                var l = polygons[id];
                l.setStyle({
                    fillColor: 'yellow'

                });

                cl = l;
                cl_color = getColor();

                var popupContent = getPCONT(all_features[id]);
                l.bindPopup(popupContent, {offset: [0, -40]}).openPopup();

            };



            function getPCONT(featureSelected) {

                var cont = "<table class='table'><tr><td>WARD</td><td>" + featureSelected.properties.WARD + "</td></tr></table>";
                return cont;


            }



            function countryMouseover(feature, leafletEvent) {
                var layer = leafletEvent.target;
                layer.setStyle({
                    weight: 2,
                    color: '#666',
                    fillColor: 'white'
                });
                layer.bringToFront();
                $scope.name = feature.properties.name;
                $scope.gap = feature.properties.m_f_diff;

                $scope.femper = feature.properties.female_per;

                $scope.maleper = feature.properties.male_per;


                console.log(feature);
            }


            function getColor() {

              return "#78D261";


            }

            function resetHighlight(e) {
                geojson.resetStyle(e.target);
            }
            function style(feature)
            {
                return {
                    fillColor: getColor(),
                    weight: 1,
                    opacity: 0.7,
                    color: 'white',
                    //  dashArray: '3',
                    fillOpacity: 0.7
                };
            }


            $http.get(map_path).success(function (data, status) {
                angular.extend($scope, {
                    geojson: {
                        data: data,
                        style: style,
                        mouseout: resetHighlight,
                        resetStyleOnMouseout: true
                        , selectedCountry: {},
                        onEachFeature: collectMe
                    }
                });
                $scope.features = data.properties;
            });



            $http.get(ward_label).success(function (data, status) {
                angular.extend($scope, {
                  markers: data,
                });
            });


        })
