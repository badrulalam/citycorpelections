angular.module('app')
.controller('MayoralStateController',function($scope, $stateParams, $http,$state){

var city = $stateParams.city;
$scope. city = city;

$http.get("/candidate?sort=vote_received desc&type=mayor&city="+ city)
  .success(function(data) {
    $scope.candidate_result = data;
  });
  $http.get("/candidate/getTotalVote?city="+ city)
    .success(function(data) {

      $scope.total_vote =  data[0].vote_received;

    });

});
