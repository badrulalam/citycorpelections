angular.module('app')
  .controller('AdminController', function($scope, $state, Auth, CurrentUser, LocalService, $http) {
  	if (!LocalService.get('auth_token')) {

        $state.go('login');

    }
console.log(CurrentUser);
    $scope.isCollapsed = true;
    $scope.auth = Auth;
    $scope.user = CurrentUser.user;

    $http.get("/json/dnccward.json")
      .success(function (data) {
        $scope.dnccward = data;
      });
      $http.get("/json/dsccward.json")
        .success(function (data) {
          $scope.dsccward = data;
        });
        $http.get("/json/cccward.json")
          .success(function (data) {
            $scope.cccward = data;
          });



    $scope.logout = function() {alert('logout');
      Auth.logout();
      $state.go('login');
    }
  });
