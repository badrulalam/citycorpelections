angular.module('app')
.controller('NewsEditController',function($scope, $stateParams, $http,$state){
var news = $stateParams.slug;
	$http.get('/news/'+news).success(function(response){
		$scope.News = response;
	});
	$scope.editNews = function()
	{
		console.log($scope.News);
		$http.put('/news/update/'+$scope.News.id + '?', $scope.News)
			.success(function(data, status, headers, config){
	      $state.go('admin.news.showall');
				
				
			});
	};
});