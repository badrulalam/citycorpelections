angular.module('app')
.controller('ElectionTimelineController',function($scope, $stateParams, $http){
  $scope.data = "Data Comming Soon";
  $scope.timelineShow = function(id)
  {

    var timeline = {
      first_event_date     : 'first_event',
      second_event_date     : 'second_event',
      third_event_date     : 'third_event',
      fourth_event_date     : 'fourth_event',
      april_six_event_date     : 'april_six_event',
      fifth_event_date     : 'april_nine_event',
      april_nine_event_date     : 'april_ten_event',
      seventh_event_date     : 'april_twosix_event',
      eighth_event_date     : 'april_twoeight_event'
    };
    for (var i in timeline){
      if(i==id)
      {
        $("#"+timeline[i]).removeClass('zoomOut').addClass('zoomIn');
          $("#"+timeline[i]).css("display","block");
        }
    }

  }
  $scope.timelineHide = function(id)
  {

    var timeline = {
      first_event_date     : 'first_event',
      second_event_date     : 'second_event',
      third_event_date     : 'third_event',
      fourth_event_date     : 'fourth_event',
      april_six_event_date     : 'april_six_event',
      fifth_event_date     : 'april_nine_event',
      april_nine_event_date     : 'april_ten_event',
      seventh_event_date     : 'april_twosix_event',
      eighth_event_date     : 'april_twoeight_event'
    };
    for (var i in timeline){
      if(i==id)
      {
        $("#"+timeline[i]).removeClass('zoomIn').addClass('zoomOut');
        }
    }
  }
});
