angular.module('app')
.controller('CouncilorCityWardController',function($scope, $stateParams, $http,$state){
  var city = $stateParams.city;
  var ward = $stateParams.ward;
  $http.get("/candidate?city="+ city+"&type=council&ward="+ward)
  .success(function(response){
		$scope.candidateList = response;
	});

  if(city == 'dscc')
  {
    $http.get("/json/dsccward.json")
      .success(function (data) {
        for (var i=0 ; i < data.length ; i++)
       {

             if (data[i].id == ward) {
               $scope.ward = data[i];

             }
        }



      });
  }
  if(city == 'dncc')
  {
    $http.get("/json/dnccward.json")
      .success(function (data) {
        for (var i=0 ; i < data.length ; i++)
       {

             if (data[i].id == ward) {
               $scope.ward = data[i];

             }
        }
      });
  }
  if(city == 'ccc')
  {
    $http.get("/json/cccward.json")
      .success(function (data) {
        for (var i=0 ; i < data.length ; i++)
       {

             if (data[i].id == ward) {
               $scope.ward = data[i];

             }
        }
      });
  }
  $scope.deleteCandidate = function(malp) {
    console.log(malp);
    var result = confirm("Want to delete?");
    if (result) {
      $http.post('/candidate/destroy/' + malp.id)
        .success(function(data, status, headers, config) {
          $http.get("/candidate")
            .success(function(data) {
              $scope.candidateList = data;
            });
        });

    }
  };





});
