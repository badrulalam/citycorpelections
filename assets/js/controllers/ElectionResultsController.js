angular.module('app')
.controller('ElectionResultsController',function($scope, $state,$filter,ngTableParams, $modal, $http, $location, $anchorScroll,$interval){
  $scope.tabs = [
    { title:'Dynamic Title 1', content:'Dynamic content 1' },
    { title:'Dynamic Title 2', content:'Dynamic content 2', disabled: true }
  ];

  $scope.alertMe = function() {
    setTimeout(function() {
      $window.alert('You\'ve selected the alert tab!');
    });
  };
  $scope.go = function(route){
    $state.go(route);
  };
  $scope.goCity = function(route,city){
    $state.go(route, ({city: city}));
  };

});
