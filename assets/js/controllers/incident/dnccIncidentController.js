
angular.module('app')
        .controller('dnccIncidentController', function ($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {
          var map_path = "/json/dncc_polling_stations_2015_new.geojson";

          var ward_label = "/json/ward-label-dncc.json";

          $scope.heading = "Dhaka North";





                 angular.extend($scope, {
                bd: {
                    lat: 23.7088,
                    lng: 90.40,
                    zoom: 12
                },
                defaults: {
                    scrollWheelZoom: false,
                    attributionControl: false
                },
                layers: {
                    baselayers: {
                        osm: {
                            name: 'osm',
                            url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                            type: 'xyz'
                        },

                    },

               overlays: {
                wms: {
                    name: 'CCC',
                    type: 'wms',
                    visible: true,
                    url: 'http://128.199.120.151:8080/geoserver/ewg/wms',
                    layerParams: {
                        service: 'WMS',
                        version: '1.1.0',
                        request: 'GetMap',
                        srs: 'EPSG:4326',
                        style:'whitepol',
                        layers: 'dncc-voter-population',
                        format: 'image/png',
                        transparent: true
                    }
                }
            }


                }



            });



            function countryClick(featureSelected, leafletEvent) {



                var layer = leafletEvent.target;

                var popupContent = getPCONT(layer, featureSelected);
                layer.bindPopup(popupContent).openPopup();

            }





            $scope.showme = false;
            var polygons = [];
            var all_features = [];
            var cl = false;
            var cl_color;

            function collectMe(feature, layer) {


              layer.on("click", function (e) {
                  $scope.showme = true;

                  var popupContent = getPCONT(feature);
                  var totalcenter = feature.properties.totalcenter;
                  $scope.name = feature.properties.name;
                  var pollingdata = [];
                  for(var i=1;i<= totalcenter; i++)
                  {
                      if(i==1){
                      var polling = {};
                      polling.center = "Center 1";
                      polling.voter = feature.properties.voter_1;
                      polling.booth = feature.properties.booth_1;
                      polling.sl = feature.properties.sl_1;
                      polling.type = feature.properties.type_1;
                      pollingdata[i-1] = polling;
                      }
                      if(i==2){
                      var polling = {};
                      polling.center = "Center 2";
                      polling.voter = feature.properties.voter_2;
                      polling.booth = feature.properties.booth_2;
                      polling.sl = feature.properties.sl_2;
                      polling.type = feature.properties.type_2;
                      pollingdata[i-1] = polling;
                      }
                      if(i==3){
                      var polling = {};
                      polling.center = "Center 3";
                      polling.voter = feature.properties.voter_3;
                      polling.booth = feature.properties.booth_3;
                      polling.sl = feature.properties.sl_3;
                      polling.type = feature.properties.type_3;
                      pollingdata[i-1] = polling;
                      }
                      if(i==4){
                      var polling = {};
                      polling.center = "Center 4";
                      polling.voter = feature.properties.voter_4;
                      polling.booth = feature.properties.booth_4;
                      polling.sl = feature.properties.sl_4;
                      polling.type = feature.properties.type_4;
                      pollingdata[i-1] = polling;
                      }
                      if(i==5){
                      var polling = {};
                      polling.center = "Center 5";
                      polling.voter = feature.properties.voter_5;
                      polling.booth = feature.properties.booth_5;
                      polling.sl = feature.properties.sl_5;
                      polling.type = feature.properties.type_5;
                      pollingdata[i-1] = polling;
                      }
                      if(i==6){
                      var polling = {};
                      polling.center = "Center 6";
                      polling.voter = feature.properties.voter_6;
                      polling.booth = feature.properties.booth_6;
                      polling.sl = feature.properties.sl_6;
                      polling.type = feature.properties.type_6;
                      pollingdata[i-1] = polling;
                      }
                      if(i==7){
                      var polling = {};
                      polling.center = "Center 7";
                      polling.voter = feature.properties.voter_7;
                      polling.booth = feature.properties.booth_7;
                      polling.sl = feature.properties.sl_7;
                      polling.type = feature.properties.type_7;
                      pollingdata[i-1] = polling;
                      }
                      if(i==8){
                      var polling = {};
                      polling.center = "Center 8";
                      polling.voter = feature.properties.voter_8;
                      polling.booth = feature.properties.booth_8;
                      polling.sl = feature.properties.sl_8;
                      polling.type = feature.properties.type_8;
                      pollingdata[i-1] = polling;
                      }
                      if(i==9){
                      var polling = {};
                      polling.center = "Center 9";
                      polling.voter = feature.properties.voter_9;
                      polling.booth = feature.properties.booth_9;
                      polling.sl = feature.properties.sl_9;
                      polling.type = feature.properties.type_9;
                      pollingdata[i-1] = polling;
                      }
                      if(i==10){
                      var polling = {};
                      polling.center = "Center 10";
                      polling.voter = feature.properties.voter_10;
                      polling.booth = feature.properties.booth_10;
                      polling.sl = feature.properties.sl_10;
                      polling.type = feature.properties.type_10;
                      pollingdata[i-1] = polling;
                      }

                  }
                  $scope.pollingdata = pollingdata;
                  $scope.image = feature.properties.image;
                  $scope.sbo = [];

                  for(var i in pollingdata) {
                      (function(i) {

                          $http.get('/sbo/dncc?polling='+pollingdata[i].sl).success(function (data, status) {
                            $scope.pollingdata[i].sbo = data;
                         });
                      })(i);
                  }

              });

            }







            function getPCONT(featureSelected) {

                var cont = "<table class='table'><tr><td>Name</td><td>" + featureSelected.properties.male + "</td></tr></table>";
                return cont;


            }


            function plotLayer(feature, latlng){
        return L.circleMarker(latlng, {
          radius: 8,
          fillColor: "#ff7800",
          color: "#000",
          weight: 1,
          opacity: 1,
          fillOpacity: 0.8
        });
        };
        var ward = "21";
        function filterLayer(feature, latlng){
          return feature.properties.ward == ward;
        };






        var ward;

        function filterLayer(feature, latlng) {
            console.log(data);
            return feature.properties.ward == ward;
        };
        $scope.loadPS = function(w) {
            ward = w;
            $http.get(map_path).success(function(data, status) {

                angular.extend($scope, {
                    geojson: {
                        data: data,
                        selectedCountry: {},
                        onEachFeature: collectMe,
                        filter: filterLayer,
                        pointToLayer: function(feature, latlng) {
                            return L.circleMarker(latlng, {
                                radius: 8,
                                fillColor: "#ff7800",
                                color: "#000",
                                weight: 1,
                                opacity: 1,
                                fillOpacity: 0.8
                            });
                        },

                    }
                });
                $scope.features = data.properties;
            });


        };


    $scope.getColor = function(iid)
    {
      var fill = "#ff7800";
      if(iid==1)
      {
        fill = "#000080";
      }
      if(iid==2)
      {
        fill = "#006400";
      }
      if(iid==3)
      {
        fill = "#800000";
      }
      if(iid==4)
      {
        fill = "#9400D3";
      }
      if(iid==5)
      {
        fill = "#A52A2A";
      }
      if(iid==6)
      {
        fill = "#009933";
      }
      if(iid==7)
      {
        fill = "#000066";
      }
      if(iid==8)
      {
        fill = "#800000";
      }
      if(iid==9)
      {
        fill = "#720038";
      }
      if(iid==10)
      {
        fill = "#002124";
      }
      if(iid==11)
      {
        fill = "#331A66";
      }
      if(iid==12)
      {
        fill = "#006400";
      }
      if(iid==13)
      {
        fill = "#3D143D";
      }
      if(iid==14)
      {
        fill = "#001A33";
      }
      if(iid==15)
      {
        fill = "#9400D3";
      }
      if(iid==16)
      {
        fill = "#003300";
      }
      return fill;
    };
    $http.get('/json/dnccIncident.json').success(function(data, status) {
      var markers = [];
      var counter = 0;
      for(var i=0;i< data.length; i++)
      {
        if(data[i].Latitude != "") {
            var ico = {};

            var lat = parseFloat(data[i].Latitude);
            var lng = parseFloat(data[i].Longitude);


            var facolor = $scope.getColor(data[i].ans_no);
            console.log(facolor);
            ico = {
              lat: lat,
              lng: lng,
              "icon": {
                "type": "div",
                "iconSize": [20, 20],
                "html": "<span class='step' style='background-color: "+ facolor+ " '>"+data[i].totalIncidents+"</span>",
                "popupAnchor": [0, 0]
              },
              focus: true,
              draggable: false
            };
            markers[counter++] = ico;
        }
      }

      angular.extend($scope, {
        markers: markers,

      });

    });
    $scope.showThisIncident = function(incidentId){
      $('.incident-list li').each(function(){
        $(this).removeClass();
        if($(this).data("incident")==incidentId)
          $(this).addClass("selectedIncident");
      });
      $http.get('/json/dnccIncident.json').success(function (data, status) {
        var markers = [];
        var counter = 0;
        for(var i=0;i< data.length; i++)
        {
          if(data[i].ans_no == incidentId) {

          if(data[i].Latitude != "") {
              var ico = {};

              var lat = parseFloat(data[i].Latitude);
              var lng = parseFloat(data[i].Longitude);


              var facolor = $scope.getColor(data[i].ans_no);
              console.log(facolor);
              ico = {
                lat: lat,
                lng: lng,
                "icon": {
                  "type": "div",
                  "iconSize": [20, 20],
                  "html": "<span class='step' style='background-color: "+ facolor+ " '>"+data[i].totalIncidents+"</span>",
                  "popupAnchor": [0, 0]
                },
                focus: true,
                draggable: false
              };
              markers[counter++] = ico;
          }}
        }

        angular.extend($scope, {
          markers: markers,

        });

      });
    };
$scope.showAllIncident = function()
{
  $http.get('/json/dnccIncident.json').success(function(data, status) {
    var markers = [];
    var counter = 0;
    for(var i=0;i< data.length; i++)
    {
      if(data[i].Latitude != "") {
          var ico = {};

          var lat = parseFloat(data[i].Latitude);
          var lng = parseFloat(data[i].Longitude);


          var facolor = $scope.getColor(data[i].ans_no);
          console.log(facolor);
          ico = {
            lat: lat,
            lng: lng,
            "icon": {
              "type": "div",
              "iconSize": [20, 20],
              "html": "<span class='step' style='background-color: "+ facolor+ " '>"+data[i].totalIncidents+"</span>",
              "popupAnchor": [0, 0]
            },
            focus: true,
            draggable: false
          };
          markers[counter++] = ico;
      }
    }

    angular.extend($scope, {
      markers: markers,

    });

  });}

        });
