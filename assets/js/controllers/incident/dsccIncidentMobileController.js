angular.module('app')
    .controller('dsccIncidentMobileController', function($scope, $filter, $document, $location, $anchorScroll, ngTableParams, $state, $http, leafletData, leafletMapDefaults, leafletHelpers, leafletEvents) {
        $scope.heading = "Dhaka South";
        var map_path = "/json/dsccfinal6.geojson";
        var ward_label = "/json/ward-label-dscc.json";

        angular.extend($scope, {
            bd: {
                lat: 23.6450,
                lng: 90.42,
                zoom: 12
            },
            defaults: {
                scrollWheelZoom: false,
                attributionControl: false
            },
            layers: {
                baselayers: {
                    osm: {
                        name: 'osm',
                        url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        type: 'xyz'
                    },
                },
            },

        });

        function countryClick(featureSelected, leafletEvent) {
            var layer = leafletEvent.target;

            var popupContent = getPCONT(layer, featureSelected);
            layer.bindPopup(popupContent).openPopup();

        }




        $scope.showme = false;
        var polygons = [];
        var all_features = [];
        var cl = false;
        var cl_color;

        function collectMe(feature, layer) {

            if (feature.geometry.type === 'MultiPolygon') {
                // Don't stroke and do opaque fill
                layer.setStyle({


                    fillColor: '#444',
                    weight: 1,
                    opacity: 0.7,
                    color: 'white',
                    //  dashArray: '3',
                    fillOpacity: 0.7


                });
            }

            layer.on("click", function(e) {
                $scope.showme = true;

                var popupContent = getPCONT(feature);
                //console.log(feature.properties);
                var totalcenter = feature.properties.totalcenter;
                $scope.name = feature.properties.name;
                var pollingdata = [];
                for (var i = 1; i <= totalcenter; i++) {
                    if (i == 1) {
                        var polling = {};
                        polling.center = "Center 1";
                        polling.voter = feature.properties.voter_1;
                        polling.booth = feature.properties.booth_1;
                        polling.sl = feature.properties.sl_1;
                        polling.type = feature.properties.type_1;
                        pollingdata[i - 1] = polling;
                    }
                    if (i == 2) {
                        var polling = {};
                        polling.center = "Center 2";
                        polling.voter = feature.properties.voter_2;
                        polling.booth = feature.properties.booth_2;
                        polling.sl = feature.properties.sl_2;
                        polling.type = feature.properties.type_2;
                        pollingdata[i - 1] = polling;
                    }
                    if (i == 3) {
                        var polling = {};
                        polling.center = "Center 3";
                        polling.voter = feature.properties.voter_3;
                        polling.booth = feature.properties.booth_3;
                        polling.sl = feature.properties.sl_3;
                        polling.type = feature.properties.type_3;
                        pollingdata[i - 1] = polling;
                    }
                    if (i == 4) {
                        var polling = {};
                        polling.center = "Center 4";
                        polling.voter = feature.properties.voter_4;
                        polling.booth = feature.properties.booth_4;
                        polling.sl = feature.properties.sl_4;
                        polling.type = feature.properties.type_4;
                        pollingdata[i - 1] = polling;
                    }
                    if (i == 5) {
                        var polling = {};
                        polling.center = "Center 5";
                        polling.voter = feature.properties.voter_5;
                        polling.booth = feature.properties.booth_5;
                        polling.sl = feature.properties.sl_5;
                        polling.type = feature.properties.type_5;
                        pollingdata[i - 1] = polling;
                    }
                    if (i == 6) {
                        var polling = {};
                        polling.center = "Center 6";
                        polling.voter = feature.properties.voter_6;
                        polling.booth = feature.properties.booth_6;
                        polling.sl = feature.properties.sl_6;
                        polling.type = feature.properties.type_6;
                        pollingdata[i - 1] = polling;
                    }

                }
                $scope.pollingdata = pollingdata;
                $scope.image = feature.properties.Image;

                $scope.sbo = [];

                for (var i in pollingdata) {
                    (function(i) {

                        $http.get('/sbo/dscc?polling=' + pollingdata[i].sl).success(function(data, status) {
                            $scope.pollingdata[i].sbo = data;
                        });
                    })(i);
                }



            });
        }



        $http.get(map_path).success(function(data, status) {

            angular.extend($scope, {
                geojson: {
                    data: data,
                    selectedCountry: {},
                    onEachFeature: collectMe,
                    pointToLayer: function(feature, latlng) {
                      return L.circleMarker(latlng, {
                        radius: 8,
                        fillColor: "#ff7800",
                        color: "#000",
                        weight: 1,
                        opacity: 1,
                        fillOpacity: 0.8
                      });
                    }

                }
            });
            $scope.features = data.properties;
        });
        $scope.selectMe = function(id) {

            toMap();

            if (cl) {
                var MouseOutStyle = {
                    weight: 1,
                    fillColor: cl_color
                };

                cl.setStyle(MouseOutStyle);
            }
            $location.hash('map');

            //  $anchorScroll();

            var l = polygons[id];
            l.setStyle({
                fillColor: 'yellow'

            });

            cl = l;
            cl_color = getColor(all_features[id].WARD);

            var popupContent = getPCONT(all_features[id]);
            l.bindPopup(popupContent, {
                offset: [0, -40]
            }).openPopup();

        };



        function getPCONT(featureSelected) {

            var cont = "<table class='table'><tr><td>WARD</td><td>" + featureSelected.properties.WARD + "</td></tr></table>";
            return cont;


        }




        var var_marker = {
            osloMarker: {
                lat: 23.6358,
                lng: 90.42,
                message: "I want to travel here!",
                focus: true,
                draggable: false
            },
            osloMarsker: {
                lat: 23.6358,
                lng: 90.45,
                message: "I want to travel here!",
                focus: true,
                draggable: false
            },

        };

        var var_marker1 = {
            osloMarker: {
                lat: 23.6388,
                lng: 90.42,
                message: "I want to travel here!",
                focus: true,
                draggable: false
            },
            osloMarsker: {
                lat: 23.6338,
                lng: 90.45,
                message: "I want to travel here!",
                focus: true,
                draggable: false
            },

        };

        function style(feature) {
            return {
                fillColor: '#444',
                weight: 1,
                opacity: 0.7,
                color: 'white',
                //  dashArray: '3',
                fillOpacity: 0.7
            };
        }
        var ward;

        function filterLayer(feature, latlng) {
          // console.log(ward);
          // console.log(feature);

            return feature.properties.ward == ward;
        };
        function filterPolygon(feature, latlng) {
            console.log(feature.properties.ward);
            return feature.properties.ward == ward;
        };



        // var div_icon = [];
        // for(var i=0;i< 100; i++)
        // {
        //   var ico = {};
        //   ico = {
        //     type: 'div',
        //     iconSize: [20, 20],
        //     html: '<b>'+i+'</b>',
        //     popupAnchor:  [0, 0]
        //   };
        //   div_icon[i]=ico;
        // }



    $scope.getColor = function(iid)
        {
          var fill = "#ff7800";
          if(iid==1)
          {
            fill = "#000080";
          }
          if(iid==2)
          {
            fill = "#006400";
          }
          if(iid==3)
          {
            fill = "#800000";
          }
          if(iid==4)
          {
            fill = "#9400D3";
          }
          if(iid==5)
          {
            fill = "#A52A2A";
          }
          if(iid==6)
          {
            fill = "#009933";
          }
          if(iid==7)
          {
            fill = "#000066";
          }
          if(iid==8)
          {
            fill = "#800000";
          }
          if(iid==9)
          {
            fill = "#720038";
          }
          if(iid==10)
          {
            fill = "#002124";
          }
          if(iid==11)
          {
            fill = "#331A66";
          }
          if(iid==12)
          {
            fill = "#006400";
          }
          if(iid==13)
          {
            fill = "#3D143D";
          }
          if(iid==14)
          {
            fill = "#001A33";
          }
          if(iid==15)
          {
            fill = "#9400D3";
          }
          if(iid==16)
          {
            fill = "#003300";
          }
          return fill;
        };
        $scope.loadPS = function(w) {
            ward = w;
            iid = w;
            $http.get(map_path).success(function(data, status) {

                angular.extend($scope, {
                    geojson: {
                        data: data,
                        selectedCountry: {},
                        onEachFeature: collectMe,
                        filter: filterLayer,
                        pointToLayer: pointToLayer,

                    }
                });
                $scope.features = data.properties;
            });


        };

    $http.get('/json/dsccIncident.json').success(function(data, status) {

      var markers = [];
      var counter = 0;
      for(var i=0;i< data.length; i++)
      {
        if(data[i].Latitude != "") {
            var ico = {};

            var lat = parseFloat(data[i].Latitude);
            var lng = parseFloat(data[i].Longitude);


            var facolor = $scope.getColor(data[i].ans_no);
            console.log(facolor);
            ico = {
              lat: lat,
              lng: lng,
              "icon": {
                "type": "div",
                "iconSize": [20, 20],
                "html": "<span class='step' style='background-color: "+ facolor+ " '>"+data[i].totalIncidents+"</span>",
                "popupAnchor": [0, 0]
              },
              focus: true,
              draggable: false
            };
            markers[counter++] = ico;
        }
      }

      angular.extend($scope, {
        markers: markers,

      });

    });
        $scope.showThisIncident = function(incidentId) {
            $('.incident-list li').each(function() {
                $(this).removeClass();
                if ($(this).data("incident") == incidentId)
                    $(this).addClass("selectedIncident");
            });
             $http.get('/json/dsccIncident.json').success(function(data, status) {
               var markers = [];
               var counter = 0;
               for(var i=0;i< data.length; i++)
               {
                 if(data[i].ans_no == incidentId) {
                 if(data[i].Latitude != "") {
                     var ico = {};

                     var lat = parseFloat(data[i].Latitude);
                     var lng = parseFloat(data[i].Longitude);


                     var facolor = $scope.getColor(data[i].ans_no);
                     console.log(facolor);
                     ico = {
                       lat: lat,
                       lng: lng,
                       "icon": {
                         "type": "div",
                         "iconSize": [20, 20],
                         "html": "<span class='step' style='background-color: "+ facolor+ " '>"+data[i].totalIncidents+"</span>",
                         "popupAnchor": [0, 0]
                       },
                       focus: true,
                       draggable: false
                     };
                     markers[counter++] = ico;
                 }
               }
             }
               angular.extend($scope, {
                 markers: markers,

               });


             });
        };


    });
