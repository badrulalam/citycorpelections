angular.module('app')
  .controller('HomeController', function($scope, $state, $filter, ngTableParams, $http, $location, $anchorScroll,$interval,$translate) {

    $http.get('/news').success(function (data) {
      $scope.data = data;


      $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 100, // count per page
        sorting: {
          date: 'desc'     // initial sorting
        }
      }, {
        total: data.length, // length of data
        getData: function ($defer, params) {
          // use build-in angular filter
          var orderedData = params.sorting() ?
            $filter('orderBy')(data, params.orderBy()) :
            data;

          $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
      });


    });




    $http.get("/news?featured=true")
      .success(function (data) {
        $scope.ticker_data =data;
        $scope.news =data;


      });
       $http.get("/candidate?city=ccc&type=mayor")
            .success(function (data) {
              $scope.ccc_candidates = $scope.shuffle(data);
            });

    $http.get("/candidate?city=dncc&type=mayor")
      .success(function (data) {
        $scope.dncc_candidates = $scope.shuffle(data);
      });

    $http.get("/candidate?city=dscc&type=mayor")
      .success(function (data) {
        $scope.dscc_candidates = $scope.shuffle(data);
      });


    angular.element(document).ready(function(){

      jQuery.fn.liScroll = function(settings) {
        settings = jQuery.extend({
          travelocity: 0.07
        }, settings);
        return this.each(function(){

          var $strip = jQuery(this);




          $strip.addClass("newsticker")
          var stripWidth = 1;
          $strip.find("li").each(function(i){
            stripWidth += jQuery(this, i).outerWidth(true); // thanks to Michael Haszprunar and Fabien Volpi
          });
          var $mask = $strip.wrap("<div class='mask'></div>");
          var $tickercontainer = $strip.parent().wrap("<div class='tickercontainer'></div>");
          var containerWidth = $strip.parent().parent().width();	//a.k.a. 'mask' width
          $strip.width(stripWidth);
          var totalTravel = stripWidth+containerWidth;
          var defTiming = totalTravel/settings.travelocity;	// thanks to Scott Waye
          function scrollnews(spazio, tempo){
            $strip.animate(
              {left: '-='+ spazio},
              tempo,
              "linear",
              function(){
                $strip.css("left", containerWidth);
                console.log("totaltravel ");
                console.log(totalTravel);
                scrollnews(totalTravel, defTiming);
              }
            );
          }
          scrollnews(totalTravel, defTiming);
          //$strip.hover(function(){
          //    //jQuery(this).stop();
          //    $strip.stop();
          //},
          //function(){
          //    var offset = jQuery(this).offset();
          //    var residualSpace = offset.left + stripWidth;
          //    var residualTime = residualSpace/settings.travelocity;
          //    scrollnews(residualSpace, residualTime);
          //});

          $("#pause-resume").click(function(e) {
            e.preventDefault();
            console.log($("#pause-resume").data("toggle"));
            if($("#pause-resume").data("toggle")=="pause"){
              $strip.stop();
              $("#pause-resume").data("toggle","resume");
              $("#pause-resume").html("Resume");
            }else{
              var offset = jQuery(this).offset();
              var residualSpace = offset.left + stripWidth;
              var residualTime = residualSpace/settings.travelocity;
              scrollnews(residualSpace, residualTime);
              $("#pause-resume").data("toggle","pause");
              $("#pause-resume").html("Pause");
            }

          });

        });
      };

      //$("ul#ticker01").liScroll({travelocity: 0.15});
      $scope.news = $scope.ticker_data;
      $interval($scope.news_move ,25);
      //$('#timer_dhaka_north').countdown({
      //  date: '4/28/2015 10:00:00',
      //  offset: -8,
      //  day: 'Day',
      //  days: 'Days'
      //}, function () {
      //
      //});
      //
      //$('#timer_dhaka_south').countdown({
      //  date: '4/28/2015 10:00:00',
      //  offset: -8,
      //  day: 'Day',
      //  days: 'Days'
      //}, function () {
      //});
      //
      //$('#timer_chittagong').countdown({
      //  date: '4/28/2015 10:00:00',
      //  offset: -8,
      //  day: 'Day',
      //  days: 'Days'
      //}, function () {
      //});
      //
      //$('#timer_candidate_elec').countdown({
      //  date: '4/28/2015 2:00:00',
      //  offset: -8,
      //  day: 'Day',
      //  days: 'Days'
      //}, function () {
      //});

    });

    //$scope.timerInit = function()
    //{
    //  $('#timer_dhaka_north').countdown({
    //    date: '4/28/2015 10:00:00',
    //    offset: -8,
    //    day: 'Day',
    //    days: 'Days'
    //  }, function () {
    //  });
    //
    //  $('#timer_dhaka_south').countdown({
    //    date: '4/28/2015 10:00:00',
    //    offset: -8,
    //    day: 'Day',
    //    days: 'Days'
    //  }, function () {
    //  });
    //
    //  $('#timer_chittagong').countdown({
    //    date: '4/28/2015 10:00:00',
    //    offset: -8,
    //    day: 'Day',
    //    days: 'Days'
    //  }, function () {
    //  });
    //
    //  $('#timer_candidate_elec').countdown({
    //    date: '4/28/2015 2:00:00',
    //    offset: -8,
    //    day: 'Day',
    //    days: 'Days'
    //  }, function () {
    //  });
    //}
    $scope.myInterval = 4000;
    $scope.listdata = ['sdf','sdsaaaaa','dfstra'];
    var slides = $scope.slides = [];
    $scope.changeTab = function(id) {
      document.getElementById('chittagong').className = "tab-pane";
      document.getElementById('north').className = "tab-pane";
      document.getElementById('south').className = "tab-pane";
      if(id=="chittagong")
      {
        document.getElementById('tab_button_ctg').className = "tabheader active";
        document.getElementById('tab_button_north').className = "tabheader ";
        document.getElementById('tab_button_south').className = "tabheader ";
      }
      if(id=="north")
      {
        document.getElementById('tab_button_ctg').className = "tabheader";
        document.getElementById('tab_button_north').className = "tabheader active";
        document.getElementById('tab_button_south').className = "tabheader ";
      }
      if(id=="south")
      {

        document.getElementById('tab_button_ctg').className = "tabheader";
        document.getElementById('tab_button_north').className = "tabheader";
        document.getElementById('tab_button_south').className = "tabheader active";
      }
      var NAME = document.getElementById(id);
      NAME.className = "tab-pane active";
    };

    $scope.scrollTo = function(id) {
      $location.hash(id);
      $anchorScroll();
    };
    $scope.validateEmail =function (email) {
      var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    };
    $scope.addSubscriber = function() {

      if($scope.validateEmail($scope.subscriberEmail))
      {
        $http.get("/subscriber?email=" + $scope.subscriberEmail)
          .success(function(data) {
            //if(typeof data[0] != "undefined")
            if (data.length > 0) {
              $scope.subscriberEmail = "";
              $scope.subscribedMessage = "You already Subscribed!";
            }
            else
            {
              $scope.Subscriber = {};
              $scope.Subscriber.email = $scope.subscriberEmail;
              $scope.Subscriber.is_subscribe = "True";
              $http.post('/subscriber/create/?', $scope.Subscriber).
                success(function(data, status, headers, config) {
                  $scope.subscriberEmail = "";
                  $scope.subscribedMessage = "Thank you for subscribing!";
                }).
                error(function(data, status, headers, config) {

                });
            }
          });
      }
      else
      {
        $scope.subscribedMessage = "Invalid Email address!";
      }



    };
    $scope.shuffle = function(o){ for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x); return o; };

    $scope.timelineShow = function(id)
    {

      var timeline = {
        first_event_date     : 'first_event',
        second_event_date     : 'second_event',
        third_event_date     : 'third_event',
        fourth_event_date     : 'fourth_event',
        april_six_event_date     : 'april_six_event',
        fifth_event_date     : 'april_nine_event',
        april_nine_event_date     : 'april_ten_event',
        seventh_event_date     : 'april_twosix_event',
        eighth_event_date     : 'april_twoeight_event'
      };
      for (var i in timeline){
        if(i==id)
        {
          $("#"+timeline[i]).removeClass('zoomOut').addClass('zoomIn');
            $("#"+timeline[i]).css("display","block");
          }
      }

    }
    $scope.timelineHide = function(id)
    {

      var timeline = {
        first_event_date     : 'first_event',
        second_event_date     : 'second_event',
        third_event_date     : 'third_event',
        fourth_event_date     : 'fourth_event',
        april_six_event_date     : 'april_six_event',
        fifth_event_date     : 'april_nine_event',
        april_nine_event_date     : 'april_ten_event',
        seventh_event_date     : 'april_twosix_event',
        eighth_event_date     : 'april_twoeight_event'
      };
      for (var i in timeline){
        if(i==id)
        {
          $("#"+timeline[i]).removeClass('zoomIn').addClass('zoomOut');
          }
      }
    }




    $scope.news = [];
    $scope.conf = {
        news_length: false,
        news_pos: 1200, // the starting position from the right in the news container
        news_margin: 60,
        time: 10,
        news_move_flag: true,
        counter: 0
    };
    var scroll;
    var scrollr;
$scope.init = function() {
    /*$http.post('the_news_file.json', null).success(function(data) {
        if (data && data.length > 0) {*/
            //$scope.news = data;
            scroll = $interval($scope.news_move ,3);
    /*  }
    });*/
};  $scope.scrollRun = function(timer) {

              scrollr = $interval($scope.news_move_forward ,timer);

  };

    $scope.get_news_left = function(idx) {
        var $right = $scope.conf.news_pos;
        for (var ri=0; ri < idx; ri++) {
            if (document.getElementById('news_'+ri)) {
                $right += $scope.conf.news_margin + angular.element(document.getElementById('news_'+ri))[0].offsetWidth;
            }
        }
        return $right+'px';
    };
    $scope.get_news_right = function(idx) {
        var $right = $scope.conf.news_pos;
        for (var ri=0; ri < idx; ri++) {
            if (document.getElementById('news_'+ri)) {
                $right += $scope.conf.news_margin + angular.element(document.getElementById('news_'+ri))[0].offsetWidth;
            }
        }
        return $right+'px';
    };

    $scope.news_move = function() {
        if ($scope.conf.news_move_flag) {
            $scope.conf.news_pos--;

try{

              if ( angular.element(document.getElementById('news_0'))[0].offsetLeft + 600 < $scope.conf.news_margin  ) {
                  var first_new = $scope.news[0];
                $scope.news.push(first_new);
                $scope.news.shift();
                $scope.conf.news_pos += angular.element(document.getElementById('news_0'))[0].offsetWidth + $scope.conf.news_margin;
            }
          }
          catch(e)
          {

          }
        }
    };
    $scope.news_move_backward = function() {
      if ($scope.conf.counter<200) {
        $scope.conf.news_pos+=5;
        if ( angular.element(document.getElementById('news_0'))[0].offsetLeft > angular.element(document.getElementById('news_strip'))[0].offsetWidth  -2000) {
            var first_new = $scope.news[0];
            $scope.news.push(first_new);
            $scope.news.shift();
            $scope.conf.news_pos -= angular.element(document.getElementById('news_0'))[0].offsetWidth + $scope.conf.news_margin+2000;
        }

    }else{
      $interval.cancel(scrollr);
      $scope.init();
    }

    $scope.conf.counter++;
};
    $scope.news_move_forward = function() {
        if ($scope.conf.counter<200) {
            $scope.conf.news_pos-=5;

          //console.log(document.getElementById('news_0'));

              if ( angular.element(document.getElementById('news_0'))[0].offsetLeft + 600 < $scope.conf.news_margin  ) {
                  var first_new = $scope.news[0];
                $scope.news.push(first_new);
                $scope.news.shift();
                $scope.conf.news_pos += angular.element(document.getElementById('news_0'))[0].offsetWidth + $scope.conf.news_margin;
            }
        }
        else{
          $interval.cancel(scrollr);
          $scope.init();
        }
        $scope.conf.counter++;
    };
    $scope.scrollForward = function()
    {
      $interval.cancel(scroll);
      $interval.cancel(scrollr);
      $scope.conf.counter = 0;
       scrollr = $interval($scope.news_move_forward ,5);

    }
    $scope.scrollBackward = function()
    {
      $interval.cancel(scroll);
      $interval.cancel(scrollr);
      $scope.conf.counter = 0;
       scrollr = $interval($scope.news_move_backward ,5);

    }






    $scope.addSlide = function() {
      var newWidth = 600 + slides.length + 1;
      slides.push({
        image: 'http://placekitten.com/' + newWidth + '/300',
        text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
        ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
      });
    };
    for (var i=0; i<4; i++) {
      $scope.addSlide();
    }

  });
