//angular.module('app', ['pascalprecht.translate'])
//  .config(function($translateProvider) {
//    // Our translations will go in here
//    $translateProvider
//
//      .translations('en', {
//      HEADLINE: 'Hello there, This is my awesome app!',
//      INTRO_TEXT: 'And it has i18n support!'
//      })
//
//      .translations('bn', {
//        HEADLINE: 'Hey, das ist meine großartige App!',
//        INTRO_TEXT: 'Und sie untersützt mehrere Sprachen!'
//      });
//
//    $translateProvider.preferredLanguage('en');
//  });

//var app = angular.module('app', ['pascalprecht.translate']);
//app.config(['$translateProvider', function ($translateProvider) {
//  console.log("ekhane aise");
//  $translateProvider.translations('en', {
//    '2015_CITY_CORPORATION_ELECTIONS': 'Hello',
//    'FOO': 'This is a paragraph',
//    'BUTTON_LANG_EN':"EN",
//    'BUTTON_LANG_BN':"BN"
//  });
//
//  $translateProvider.translations('bn', {
//    '2015_CITY_CORPORATION_ELECTIONS': '2015',
//    'FOO': 'Dies ist ein Paragraph',
//    'BUTTON_LANG_EN':"EN",
//    'BUTTON_LANG_BN':"BN"
//  });
//
//  $translateProvider.preferredLanguage('en');
//  $translateProvider.uses('en');
//  //$translateProvider.rememberLanguage(true);
//}]);

var app = angular.module('app', ['ui.router','pascalprecht.translate','ui.bootstrap','ui.grid','duScroll','ngTable','ui.select','leaflet-directive','angularFileUpload','ngSanitize','ngCsv'])
  .run(function($rootScope, $state, Auth, $location, $window) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

      //checking if the access property is set or not, if not sent then set access = 1 means annonymous
      if(toState.hasOwnProperty('data')){

        if (!Auth.authorize(toState.data.access)) {
          event.preventDefault();

          $state.go('login');
        }
      }


    });

     $rootScope
    .$on('$stateChangeSuccess',
        function(event){

            if (!$window.ga)
                return;

            $window.ga('send', 'pageview', { page: $location.path() });
    });
  });
  



