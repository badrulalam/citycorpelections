angular.module('app')
        .config(function ($stateProvider, $urlRouterProvider, AccessLevels) {

            $stateProvider

                    .state('home', {
                        url: '/',
                        templateUrl: '/templates/home.html',
                        controller: 'HomeController',
                        data: {
                          access: AccessLevels.anon
                        }
                    })
                    .state('home_maps', {
                        url: '/',
                        templateUrl: '/templates/home.html',
                        controller: 'HomeController',
                        data: {
                          access: AccessLevels.anon
                        },
                        onEnter: function () {
angular.element(document).ready(function(){
                            $('html, body').animate({
    scrollTop: $("#map_container_h1").offset().top
}, 2000);});

 }
                    })

                    .state('about_citycorp', {
                        url: '/about-citycorp',
                        templateUrl: 'templates/about_citycorp.html',
                        controller: 'AboutCitycorpController'

                    })
                    .state('election_obserber', {
                        url: '/election-obserber',
                        templateUrl: 'templates/election_obserber.html',
                        controller: 'ElectionObserberController'

                    })
                    .state('dhakathon', {
                        url: '/dhakathon',
                        templateUrl: 'templates/dhakathon.html',
                        controller: 'dhakathonController'

                    })
                    .state('electiontimeline', {
                        url: '/electiontimeline',
                        templateUrl: 'templates/electiontimeline.html',
                        controller: 'ElectionTimelineController'

                    })
                    .state('election_results', {
                        url: '/election-results',
                        templateUrl: 'templates/election_results.html',
                        controller: 'ElectionResultsController'

                    })
                    .state('election_results.mayoral', {
                        url: '/mayoral',
                        templateUrl: 'templates/election_result/mayoral.html',
                        //controller: 'cccVoterController'
                    })
                    .state('election_results.mayoral.state', {
                        url: '/state/:city',
                        templateUrl: 'templates/election_result/mayoral_state.html',
                        controller: 'MayoralStateController'
                    })
                    .state('election_results.councilor', {
                        url: '/councilor',
                        templateUrl: 'templates/election_result/councilor.html',
                        //controller: 'cccVoterController'
                    })
                    .state('election_results.councilor.state', {
                        url: '/state/:city',
                        templateUrl: 'templates/election_result/councilor_state.html',
                        controller: 'CouncilorStateController'
                    })
                    .state('election_results.reserved', {
                        url: '/reserved',
                        templateUrl: 'templates/election_result/reserved.html',
                        //controller: 'cccVoterController'
                    })
                    .state('election_results.reserved.state', {
                        url: '/state/:city',
                        templateUrl: 'templates/election_result/reserved_state.html',
                        controller: 'ReservedStateController'
                    })
                    .state('election_results.key_number', {
                        url: '/key-number',
                        templateUrl: 'templates/election_result/key_number.html',
                        //controller: 'cccVoterController'
                    })
                    .state('election_results.key_number.state', {
                        url: '/state/:city',
                        templateUrl: 'templates/election_result/key_number_state.html',
                        controller: 'KeyNumberStateController'
                    })
                    .state('observation_results', {
                        url: '/observation-results',
                        templateUrl: 'templates/observation_results.html',
                        controller: 'ObservationResultsController'

                    })
                    .state('incident_reports', {
                        url: '/incident-reports',
                        templateUrl: 'templates/incident_reports.html',
                        controller: 'IncidentReportsController'

                    })
                    .state('incident_reports.dsccpolling', {
                        url: '/dsccpolling',
                        templateUrl: 'templates/election_result/incident_map.html',
                        controller: 'dsccIncidentController'
                    })
                    .state('incident_reports.dsccpollingMobile', {
                        url: '/dsccpollingMobile',
                        templateUrl: 'templates/election_result/incidentMobile_map.html',
                        controller: 'dsccIncidentMobileController'
                    })
                    .state('incident_reports.dnccpolling', {
                        url: '/dnccpolling',
                        templateUrl: 'templates/election_result/incident_map.html',
                        controller: 'dnccIncidentController'
                    })
                    .state('incident_reports.dnccpollingMobile', {
                        url: '/dnccpollingMobile',
                        templateUrl: 'templates/election_result/incidentMobile_map.html',
                        controller: 'dnccIncidentMobileController'
                    })
                    .state('incident_reports.cccpolling', {
                        url: '/cccpolling',
                        templateUrl: 'templates/election_result/incident_map.html',
                        controller: 'cccIncidentController'
                    })
                    .state('incident_reports.cccpollingMobile', {
                        url: '/cccpollingMobile',
                        templateUrl: 'templates/election_result/incidentMobile_map.html',
                        controller: 'cccIncidentMobileController'
                    })
                    .state('maps', {
                        url: '/maps',
                        templateUrl: 'templates/maps.html',
                    })

                    .state('cccmap', {
                        url: '/cccmap',
                        templateUrl: 'templates/cccmap.html',
                        controller: 'cccmapController'
                    })
                    .state('cccvotermap', {
                        url: '/cccvotermap',
                        templateUrl: 'templates/cccvotermap.html',
                        controller: 'cccvotermapController'
                    })


                    .state('dnccmap', {
                        url: '/dnccmap',
                        templateUrl: 'templates/dnccmap.html',
                        controller: 'dnccmapController'
                    })
                    .state('dsccmap', {
                        url: '/dsccmap',
                        templateUrl: 'templates/dsccmap.html',
                        controller: 'dsccmapController'
                    })

                    .state('map', {
                        url: '/map',
                        templateUrl: 'templates/maps/map.html',
                    })


                    .state('map.dscc', {
                        url: '/dscc',
                        templateUrl: 'templates/maps/dscc/menu.html',
                        controller: 'dsccmapController'
                    })
                    .state('map.dscc.map', {
                        url: '/map',
                        templateUrl: 'templates/maps/dscc/map.html',
                        controller: 'dsccmapController'
                    })
                    .state('map.dsccpolling', {
                        url: '/dsccpolling',
                        templateUrl: 'templates/maps/dscc/polling.html',
                        controller: 'dsccPollingController'
                    })
                    .state('map.dsccpollingMobile', {
                        url: '/dsccpollingMobile',
                        templateUrl: 'templates/maps/dscc/pollingMobile.html',
                        controller: 'dsccPollingMobileController'
                    })
                    .state('map.dscc.population', {
                        url: '/population',
                        templateUrl: 'templates/maps/dscc/population.html',
                        controller: 'dsccPopulationController'
                    })
                    .state('map.dscc.populationMobile', {
                        url: '/populationMobile',
                        templateUrl: 'templates/maps/dscc/populationMobile.html',
                        controller: 'dsccPopulationMobileController'
                    })
                    .state('map.dscc.voter', {
                        url: '/voter',
                        templateUrl: 'templates/maps/dscc/voter.html',
                        controller: 'dsccVoterController'
                    })


                    .state('map.dncc', {
                        url: '/dncc',
                        templateUrl: 'templates/maps/dncc/menu.html',
                        controller: 'dnccmapController'
                    })
                    .state('map.dncc.map', {
                        url: '/map',
                        templateUrl: 'templates/maps/dncc/map.html',
                        controller: 'dnccmapController'
                    })
                    .state('map.dnccpolling', {
                        url: '/dnccpolling',
                        templateUrl: 'templates/maps/dncc/polling.html',
                        controller: 'dnccPollingController'
                    })
                    .state('map.dnccpollingMobile', {
                        url: '/dnccpollingMobile',
                        templateUrl: 'templates/maps/dncc/pollingMobile.html',
                        controller: 'dnccPollingMobileController'
                    })
                    .state('map.dncc.population', {
                        url: '/population',
                        templateUrl: 'templates/maps/dncc/population.html',
                        controller: 'dnccPopulationController'
                    })
                    .state('map.dncc.populationMobile', {
                        url: '/populationMobile',
                        templateUrl: 'templates/maps/dncc/populationMobile.html',
                        controller: 'dnccPopulationMobileController'
                    })
                    .state('map.dncc.voter', {
                        url: '/voter',
                        templateUrl: 'templates/maps/dncc/voter.html',
                        controller: 'dnccVoterController'
                    })
                    .state('map.dncc.voterMobile', {
                        url: '/voterMobile',
                        templateUrl: 'templates/maps/dncc/voterMobile.html',
                        controller: 'dnccVoterMobileController'
                    })


                    .state('map.ccc', {
                        url: '/ccc',
                        templateUrl: 'templates/maps/ccc/menu.html',
                        controller: 'cccmapController'
                    })
                    .state('map.ccc.map', {
                        url: '/map',
                        templateUrl: 'templates/maps/ccc/map.html',
                        controller: 'cccmapController'
                    })
                    .state('map.cccpolling', {
                        url: '/cccpolling',
                        templateUrl: 'templates/maps/ccc/polling.html',
                        controller: 'cccPollingController'
                    })
                    .state('map.cccpollingMobile', {
                        url: '/cccpollingMobile',
                        templateUrl: 'templates/maps/ccc/pollingMobile.html',
                        controller: 'cccPollingMobileController'
                    })
                    .state('map.ccc.population', {
                        url: '/population',
                        templateUrl: 'templates/maps/ccc/population.html',
                        controller: 'cccPopulationController'
                    })
                    .state('map.ccc.populationMobile', {
                        url: '/populationMobile',
                        templateUrl: 'templates/maps/ccc/populationMobile.html',
                        controller: 'cccPopulationMobileController'
                    })
                    .state('map.ccc.voter', {
                        url: '/voter',
                        templateUrl: 'templates/maps/ccc/voter.html',
                        controller: 'cccVoterController'
                    })
                    .state('map.ccc.voterMobile', {
                        url: '/voterMobile',
                        templateUrl: 'templates/maps/ccc/voterMobile.html',
                        controller: 'cccVoterMobileController'
                    })

                    .state('candidate_for_print', {
                      url: '/candidate-for-print/:slug',
                      templateUrl: 'templates/candidate_for_print.html',
                      controller: 'show_candidateController'
                    })



                    .state('councilor', {
                        url: '/councilor',
                        templateUrl: 'templates/councilor.html',
                        controller: 'CouncilorController'
                    })
                    .state('councilor.profile', {
                      url: '/profile/:slug',
                      templateUrl: 'templates/candidate_profile.html',
                      controller: 'show_candidateController'
                    })
                    .state('view_candidate', {
                        url: '/candidate',
                        templateUrl: 'templates/view_candidate.html',
                        controller: 'candidatesController'
                    })
                    .state('view_candidate.profile', {
                       url: '/profile/:slug',
                       templateUrl: 'templates/candidate_profile.html',
                       controller: 'show_candidateController',
                       onEnter: function () {
 angular.element(document).ready(function(){
                           $('html, body').animate({
   scrollTop:700
 }, 1000);});

 }
                   })

                     .state('candidates.profile', {
                        url: '/show_candidate/:slug',
                        templateUrl: 'templates/candidate_profile.html',
                        controller: 'show_candidateController'
                    })

                     .state('upload',{
                        url: '/admin/upload',
                        templateUrl: 'templates/admin/upload.html',
                        controller: 'UploadController'
                     })
                    .state('malpractice', {
                        url: '/malpractice-form',
                        templateUrl: 'templates/malpractice-form.html',
                        controller: 'MalpracticeController'
                    })
                    .state('anon.home', {
                        url: '/',
                        templateUrl: 'home.html'
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'templates/auth/login.html',
                        controller: 'LoginController'
                    })



              .state('admin', {
                        url: '/admin',
                        templateUrl: 'templates/admin/admin-panel.html',
                        controller: 'AdminController',
                        data: {
                          access: AccessLevels.admin
                        }
                    })
                    .state('admin.ward', {
                        url: '/ward',
                        controller: 'WardController',
                        templateUrl: 'templates/admin/ward/ward.html'
                    })
                    .state('admin.ward.showall', {
                        url: '/showall',
                        templateUrl: 'templates/admin/ward/ward_showall.html',
                        controller: 'WardController'
                    })
                    .state('admin.news', {
                        url: '/news',
                        controller: 'NewsController',
                        templateUrl: 'templates/admin/news/news.html'
                    })
                    .state('admin.news.showall', {
                        url: '/showall',
                        templateUrl: 'templates/admin/news/news_showall.html',
                        controller: 'NewsController'
                    })
                    .state('admin.news.create', {
                        url: '/create',
                        templateUrl: 'templates/admin/news/news_create.html',
                        controller: 'NewsController'
                    })
                    .state('admin.news.edit', {
                        url: '/edit/:slug',
                        templateUrl: 'templates/admin/news/news_edit.html',
                        controller: 'NewsEditController'
                    })
                    .state('admin.malpractice', {
                        url: '/malpractice',
                        templateUrl: 'templates/admin/malpractice/malpractice.html',
                        controller: 'MalpracticeController'
                    })
                    .state('admin.malpractice.showall', {
                        url: '/showall',
                        templateUrl: 'templates/admin/malpractice/malpractice_showall.html',
                        controller: 'MalpracticeController'
                    })
                    .state('admin.subscriber', {
                        url: '/subscriber',
                        templateUrl: 'templates/admin/subscriber/subscriber.html',
                        controller: 'SubscriberController'
                    })
                    .state('admin.subscriber.showall', {
                        url: '/showall',
                        templateUrl: 'templates/admin/subscriber/subscriber_showall.html',
                        controller: 'SubscriberController'
                    })
                    .state('admin.candidate',{
                       url: '/candidate',
                       templateUrl: 'templates/admin/candidate/candidate.html',
                       controller: 'candidatesController'
                    })
                    .state('admin.candidate.showall',{
                       url: '/showall',
                       templateUrl: 'templates/admin/candidate/candidate_showall.html',
                       controller: 'candidatesController'
                    })
                    .state('admin.candidate.showallmayoral',{
                       url: '/showallmayoral/:slug',
                       templateUrl: 'templates/admin/candidate/candidate_showallmayoral.html',
                       controller: 'candidatesMayoralController'
                    })
                    .state('admin.candidate.create',{
                       url: '/create',
                       templateUrl: 'templates/admin/candidate/candidate_create.html',
                       controller: 'candidatesController'
                    })
                    .state('admin.candidate.edit',{
                       url: '/edit/:slug',
                       templateUrl: 'templates/admin/candidate/candidate_edit.html',
                       controller: 'candidatesEditController'
                    })
                    .state('admin.candidate.pfreshowall',{
                      url: '/pfreshowall',
                      templateUrl: 'templates/admin/candidate/candidate_pfreshowall.html',
                      controller: 'candidatesPFREController'
                    })
                    .state('admin.candidate.editpfre',{
                       url: '/editpfre/:slug',
                       templateUrl: 'templates/admin/candidate/candidate_edit_pfre.html',
                       controller: 'candidatesEditPFREController'
                    })
                    .state('admin.wardcouncilor',{
                       url: '/wardcouncilor/:city/:ward',
                       templateUrl: 'templates/admin/candidate/councilor_city_ward.html',
                       controller: 'CouncilorCityWardController'
                    })
                    .state('admin.candidate.election_result',{
                       url: '/election-result',
                       templateUrl: 'templates/admin/candidate/election_result.html',
                       controller: 'AdminElectionResultController'
                    })

                    ;

            $urlRouterProvider.otherwise('/');


        })
  ;
