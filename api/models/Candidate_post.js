/**
* Candidate_post.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    post_title: {
      type: 'string'
    },
    ward: {
      type: 'string'
    },
    type: {
      type: 'string'
    },
    biography: {
      type: 'string'
    },
    image: {
      type: 'string'
    },
    date_of_birth:{
      type: 'string'
    },
    husband_father_name:{
      type: 'string'
    },
    mother_name:{
      type: 'string'
    }
  }
};

