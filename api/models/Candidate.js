module.exports = {
	 attributes: {
    name: {
      type: 'string'
    },
    city: {
      type: 'string'
    },
    type: {
      type: 'string'
    },
    type: {
      type: 'string'
    },
    biography: {
      type: 'string'
    },
    image: {
      type: 'string'
    },
     date_of_birth:{
       type: 'string'
     },
     husband_father_name:{
       type: 'string'
     },
     mother_name:{
       type: 'string'
     },
     present_address:{
       type: 'string'
     },
     permanent_address:{
       type: 'string'
     },
     fld_1_max_educational_qualifications:{
       type: 'string'
     },

     fld_2a_ami_fozdari_obhijukto_noi:{
       type: 'boolean'
     },
     //fld_2b_ami_fozdari_obhijukto_keno:{
     //  type: 'json'
     //},

     fld_3a_past_fozdari_mamla_nai:{
       type: 'boolean'
     },
     //fld_3b_past_fozdari_keno:{
     //  type: 'json'
     //},

     fld_4a_my_business_profession_desc:{
       type: 'string'
     },

     fld_5_mis_agriculture_y:{
       type: 'string'
     }, //mis = my income source; _y = yearly
     fld_5_mis_house_appartment_shop_y:{
       type: 'string'
     },
     fld_5_mis_business_paritoshik_y:{
       type: 'string'
     },
     fld_5_mis_stock_share_y:{
       type: 'string'
     },
     fld_5_mis_profession_teaching_doctor_y:{
       type: 'string'
     },
     fld_5_mis_business_job_y:{
       type: 'string'
     },
     fld_5_mis_other_y:{
       type: 'string'
     },

     fld_5_mydepis1_who:{
       type: 'string'
     }, //mydepis = my dependent income source; _y = yearly
     fld_5_mydepis1_agriculture_y:{
       type: 'string'
     }, //mis = my income source; _y = yearly
     fld_5_mydepis1_house_appartment_shop_y:{
       type: 'string'
     },
     fld_5_mydepis1_business_paritoshik_y:{
       type: 'string'
     },
     fld_5_mydepis1_stock_share_y:{
       type: 'string'
     },
     fld_5_mydepis1_profession_teaching_doctor_y:{
       type: 'string'
     },
     fld_5_mydepis1_business_job_y:{
       type: 'string'
     },
     fld_5_mydepis1_other_y:{
       type: 'string'
     },

     fld_5_mydepis2_who:{
       type: 'string'
     },
     fld_5_mydepis2_agriculture_y:{
       type: 'string'
     },
     fld_5_mydepis2_house_appartment_shop_y:{
       type: 'string'
     },
     fld_5_mydepis2_business_paritoshik_y:{
       type: 'string'
     },
     fld_5_mydepis2_stock_share_y:{
       type: 'string'
     },
     fld_5_mydepis2_profession_teaching_doctor_y:{
       type: 'string'
     },
     fld_5_mydepis2_business_job_y:{
       type: 'string'
     },
     fld_5_mydepis2_other_y:{
       type: 'string'
     },

     fld_5_mydepis3_who:{
       type: 'string'
     },
     fld_5_mydepis3_agriculture_y:{
       type: 'string'
     },
     fld_5_mydepis3_house_appartment_shop_y:{
       type: 'string'
     },
     fld_5_mydepis3_business_paritoshik_y:{
       type: 'string'
     },
     fld_5_mydepis3_stock_share_y:{
       type: 'string'
     },
     fld_5_mydepis3_profession_teaching_doctor_y:{
       type: 'string'
     },
     fld_5_mydepis3_business_job_y:{
       type: 'string'
     },
     fld_5_mydepis3_other_y:{
       type: 'string'
     },

     fld_5_mydepis4_who:{
       type: 'string'
     },
     fld_5_mydepis4_agriculture_y:{
       type: 'string'
     },
     fld_5_mydepis4_house_appartment_shop_y:{
       type: 'string'
     },
     fld_5_mydepis4_business_paritoshik_y:{
       type: 'string'
     },
     fld_5_mydepis4_stock_share_y:{
       type: 'string'
     },
     fld_5_mydepis4_profession_teaching_doctor_y:{
       type: 'string'
     },
     fld_5_mydepis4_business_job_y:{
       type: 'string'
     },
     fld_5_mydepis4_other_y:{
       type: 'string'
     },

     fld_6a_myop_cash_money:{
       type: 'string'
     }, //myop = my osthabar property
     fld_6a_myop_foreign_cur:{
       type: 'string'
     },
     fld_6a_myop_bank_arthik_protishthan:{
       type: 'string'
     },
     fld_6a_myop_bond_stock_share:{
       type: 'string'
     },
     fld_6a_myop_postal_savings:{
       type: 'string'
     },
     fld_6a_myop_bus_truck:{
       type: 'string'
     },
     fld_6a_myop_gold_other:{
       type: 'string'
     },
     fld_6a_myop_electric_equipments:{
       type: 'string'
     },
     fld_6a_myop_house_hold:{
       type: 'string'
     },
     fld_6a_myop_other:{
       type: 'string'
     },

     fld_6a_mydepop1_who:{
       type: 'string'
     }, //mydepop = my dependent osthabar property
     fld_6a_mydepop1_cash_money:{
       type: 'string'
     },
     fld_6a_mydepop1_foreign_cur:{
       type: 'string'
     },
     fld_6a_mydepop1_bank_arthik_protishthan:{
       type: 'string'
     },
     fld_6a_mydepop1_bond_stock_share:{
       type: 'string'
     },
     fld_6a_mydepop1_postal_savings:{
       type: 'string'
     },
     fld_6a_mydepop1_bus_truck:{
       type: 'string'
     },
     fld_6a_mydepop1_gold_other:{
       type: 'string'
     },
     fld_6a_mydepop1_electric_equipments:{
       type: 'string'
     },
     fld_6a_mydepop1_house_hold:{
       type: 'string'
     },
     fld_6a_mydepop1_other:{
       type: 'string'
     },

     fld_6a_mydepop2_who:{
       type: 'string'
     }, //mydepop = my dependent osthabar property
     fld_6a_mydepop2_cash_money:{
       type: 'string'
     },
     fld_6a_mydepop2_foreign_cur:{
       type: 'string'
     },
     fld_6a_mydepop2_bank_arthik_protishthan:{
       type: 'string'
     },
     fld_6a_mydepop2_bond_stock_share:{
       type: 'string'
     },
     fld_6a_mydepop2_postal_savings:{
       type: 'string'
     },
     fld_6a_mydepop2_bus_truck:{
       type: 'string'
     },
     fld_6a_mydepop2_gold_other:{
       type: 'string'
     },
     fld_6a_mydepop2_electric_equipments:{
       type: 'string'
     },
     fld_6a_mydepop2_house_hold:{
       type: 'string'
     },
     fld_6a_mydepop2_other:{
       type: 'string'
     },

     fld_6a_mydepop3_who:{
       type: 'string'
     }, //mydepop = my dependent osthabar property
     fld_6a_mydepop3_cash_money:{
       type: 'string'
     },
     fld_6a_mydepop3_foreign_cur:{
       type: 'string'
     },
     fld_6a_mydepop3_bank_arthik_protishthan:{
       type: 'string'
     },
     fld_6a_mydepop3_bond_stock_share:{
       type: 'string'
     },
     fld_6a_mydepop3_postal_savings:{
       type: 'string'
     },
     fld_6a_mydepop3_bus_truck:{
       type: 'string'
     },
     fld_6a_mydepop3_gold_other:{
       type: 'string'
     },
     fld_6a_mydepop3_electric_equipments:{
       type: 'string'
     },
     fld_6a_mydepop3_house_hold:{
       type: 'string'
     },
     fld_6a_mydepop3_other:{
       type: 'string'
     },

     fld_6a_mydepop4_who:{
       type: 'string'
     }, //mydepop = my dependent osthabar property
     fld_6a_mydepop4_cash_money:{
       type: 'string'
     },
     fld_6a_mydepop4_foreign_cur:{
       type: 'string'
     },
     fld_6a_mydepop4_bank_arthik_protishthan:{
       type: 'string'
     },
     fld_6a_mydepop4_bond_stock_share:{
       type: 'string'
     },
     fld_6a_mydepop4_postal_savings:{
       type: 'string'
     },
     fld_6a_mydepop4_bus_truck:{
       type: 'string'
     },
     fld_6a_mydepop4_gold_other:{
       type: 'string'
     },
     fld_6a_mydepop4_electric_equipments:{
       type: 'string'
     },
     fld_6a_mydepop4_house_hold:{
       type: 'string'
     },
     fld_6a_mydepop4_other:{
       type: 'string'
     },


     fld_6b_mysp_krishi_jomi:{
       type: 'string'
     }, //mysp = my sthabar property
     fld_6b_mysp_okrishi_jomi:{
       type: 'string'
     },
     fld_6b_mysp_building_residential_commercial:{
       type: 'string'
     },
     fld_6b_mysp_house_apartment:{
       type: 'string'
     },
     fld_6b_mysp_firm_tea_rabar_fish:{
       type: 'string'
     },
     fld_6b_mysp_other:{
       type: 'string'
     },

     fld_6b_mydepsp1_who:{
       type: 'string'
     }, //mydepsp = my dependent sthabar property
     fld_6b_mydepsp1_krishi_jomi:{
       type: 'string'
     },
     fld_6b_mydepsp1_okrishi_jomi:{
       type: 'string'
     },
     fld_6b_mydepsp1_building_residential_commercial:{
       type: 'string'
     },
     fld_6b_mydepsp1_house_apartment:{
       type: 'string'
     },
     fld_6b_mydepsp1_firm_tea_rabar_fish:{
       type: 'string'
     },
     fld_6b_mydepsp1_other:{
       type: 'string'
     },

     fld_6b_mydepsp2_who:{
       type: 'string'
     }, //mydepsp = my dependent sthabar property
     fld_6b_mydepsp2_krishi_jomi:{
       type: 'string'
     },
     fld_6b_mydepsp2_okrishi_jomi:{
       type: 'string'
     },
     fld_6b_mydepsp2_building_residential_commercial:{
       type: 'string'
     },
     fld_6b_mydepsp2_house_apartment:{
       type: 'string'
     },
     fld_6b_mydepsp2_firm_tea_rabar_fish:{
       type: 'string'
     },
     fld_6b_mydepsp2_other:{
       type: 'string'
     },

     fld_6b_mydepsp3_who:{
       type: 'string'
     }, //mydepsp = my dependent sthabar property
     fld_6b_mydepsp3_krishi_jomi:{
       type: 'string'
     }, //mydepsp = my dependent sthabar property
     fld_6b_mydepsp3_okrishi_jomi:{
       type: 'string'
     },
     fld_6b_mydepsp3_building_residential_commercial:{
       type: 'string'
     },
     fld_6b_mydepsp3_house_apartment:{
       type: 'string'
     },
     fld_6b_mydepsp3_firm_tea_rabar_fish:{
       type: 'string'
     },
     fld_6b_mydepsp3_other:{
       type: 'string'
     },

     fld_6b_mydepsp4_who:{
       type: 'string'
     }, //mydepsp = my dependent sthabar property
     fld_6b_mydepsp4_krishi_jomi:{
       type: 'string'
     }, //mydepsp = my dependent sthabar property
     fld_6b_mydepsp4_okrishi_jomi:{
       type: 'string'
     },
     fld_6b_mydepsp4_building_residential_commercial:{
       type: 'string'
     },
     fld_6b_mydepsp4_house_apartment:{
       type: 'string'
     },
     fld_6b_mydepsp4_firm_tea_rabar_fish:{
       type: 'string'
     },
     fld_6b_mydepsp4_other:{
       type: 'string'
     },

     fld_6c_myl_type:{
       type: 'string'
     },//myl = my liability
     fld_6c_myl_amount:{
       type: 'string'
     },

     fld_7a_myld_i_hove_no_liability:{
       type: 'boolean'
     },

     fld_7b_myld_own:{
       type: 'json'
     },
     //fld_7b_myld_own_bank_name:{
     //  type: 'string'
     //},//myld = my liability detail info
     //fld_7b_myld_own_liability_amount:{
     //  type: 'string'
     //},
     //fld_7b_myld_own_defaulter_liability_amount:{
     //  type: 'string'
     //},
     //fld_7b_myld_own_re_schedule_date:{
     //  type: 'string'
     //},
     //
     //fld_7b_myld_join_bank_name:{
     //  type: 'string'
     //},//myld = my liability detail info
     //fld_7b_myld_join_liability_amount:{
     //  type: 'string'
     //},
     //fld_7b_myld_join_defaulter_liability_amount:{
     //  type: 'string'
     //},
     //fld_7b_myld_join_re_schedule_date:{
     //  type: 'string'
     //},
     //
     //fld_7b_myld_dependent_person_bank_name:{
     //  type: 'string'
     //},//myld = my liability detail info
     //fld_7b_myld_dependent_person_liability_amount:{
     //  type: 'string'
     //},
     //fld_7b_myld_dependent_person_defaulter_liability_amount:{
     //  type: 'string'
     //},
     //fld_7b_myld_dependent_person_re_schedule_date:{
     //  type: 'string'
     //},
     //
     //fld_7b_myld_company_chairman_director_bank_name:{
     //  type: 'string'
     //},//myld = my liability detail info
     //fld_7b_myld_company_chairman_director_liability_amount:{
     //  type: 'string'
     //},
     //fld_7b_myld_company_chairman_director_defaulter_liability_amount:{
     //  type: 'string'
     //},
     //fld_7b_myld_company_chairman_director_re_schedule_date:{
     //  type: 'string'
     //}


     //probable fund receivable
     fld_pfre_1a_pfs_own:{
       type: 'string'
     }, //pfre = probable fund receivable or Expense,  pfs = probable fund Source
     fld_pfre_1a_pfs_own_amount:{
       type: 'string'
     },


     fld_pfre_1b_pfs_relative_loan_owner_name:{
       type: 'string'
     },
     fld_pfre_1b_pfs_relative_loan_owner_address:{
       type: 'string'
     },
     fld_pfre_1b_pfs_relative_loan_owner_relation:{
       type: 'string'
     },
     fld_pfre_1b_pfs_relative_loan_owner_incomesource:{
       type: 'string'
     },
     fld_pfre_1b_pfs_relative_loan_owner_amount:{
       type: 'string'
     },

     fld_pfre_1c_pfs_relative_gifter_name:{
       type: 'string'
     },
     fld_pfre_1c_pfs_relative_gifter_address:{
       type: 'string'
     },
     fld_pfre_1c_pfs_relative_gifter_relation:{
       type: 'string'
     },
     fld_pfre_1c_pfs_relative_gifter_incomesource:{
       type: 'string'
     },
     fld_pfre_1c_pfs_relative_gifter_amount:{
       type: 'string'
     },

     fld_pfre_1d_pfs_nonrelative_loan_owner_name:{
       type: 'string'
     },
     fld_pfre_1d_pfs_nonrelative_loan_owner_address:{
       type: 'string'
     },
     fld_pfre_1d_pfs_nonrelative_loan_owner_amount:{
       type: 'string'
     },

     fld_pfre_1e_pfs_nonrelative_gifter_name:{
       type: 'string'
     },
     fld_pfre_1e_pfs_nonrelative_gifter_address:{
       type: 'string'
     },
     fld_pfre_1e_pfs_nonrelative_gifter_amount:{
       type: 'string'
     },

     fld_pfre_1f_pfs_other_gifter_name:{
       type: 'string'
     },
     fld_pfre_1f_pfs_other_gifter_address:{
       type: 'string'
     },
     fld_pfre_1f_pfs_other_gifter_amount:{
       type: 'string'
     },

     //probable expenditure
     fld_pfre_2a_pe_poster_quantity:{
       type: 'string'
     },
     fld_pfre_2a_pe_poster_amount:{
       type: 'string'
     },

     fld_pfre_2b_pe_election_camp_quantity:{
       type: 'string'
     },
     fld_pfre_2b_pe_election_camp_expenditure_amount:{
       type: 'string'
     },
     fld_pfre_2b_pe_election_camp_worker_expenditure_amount:{
       type: 'string'
     },
     fld_pfre_2b_pe_election_camp_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2c_pe_central_camp_arc_expenditure_amount:{
       type: 'string'
     },
     fld_pfre_2c_pe_central_camp_worker_expenditure_amount:{
       type: 'string'
     },
     fld_pfre_2c_pe_central_camp_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2d_pe_tada_own_expenditure_amount:{
       type: 'string'
     },
     fld_pfre_2d_pe_tada_worker_expenditure_amount:{
       type: 'string'
     },
     fld_pfre_2d_pe_tada_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2e_pe_meeting_venue_expenditure_amount:{
       type: 'string'
     },
     fld_pfre_2e_pe_meeting_worker_expenditure_amount:{
       type: 'string'
     },
     fld_pfre_2e_pe_meeting_furniture_expenditure_amount:{
       type: 'string'
     },
     fld_pfre_2e_pe_meeting_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2f_pe_leaflet_press_name:{
       type: 'string'
     },
     fld_pfre_2f_pe_leaflet_quantity:{
       type: 'string'
     },
     fld_pfre_2f_pe_leaflet_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2g_pe_handbill_press_name:{
       type: 'string'
     },
     fld_pfre_2g_pe_handbill_quantity:{
       type: 'string'
     },
     fld_pfre_2g_pe_handbill_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2h_pe_sticker_press_name:{
       type: 'string'
     },
     fld_pfre_2h_pe_sticker_quantity:{
       type: 'string'
     },
     fld_pfre_2h_pe_sticker_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2i1_pe_banner_quantity:{
       type: 'string'
     },
     fld_pfre_2i1_pe_banner_prepare_amount:{
       type: 'string'
     },
     fld_pfre_2i1_pe_banner_hanging_amount:{
       type: 'string'
     },
     fld_pfre_2i1_pe_banner_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2i2_pe_dbanner_quantity:{
       type: 'string'
     },
     fld_pfre_2i2_pe_dbanner_prepare_amount:{
       type: 'string'
     },
     fld_pfre_2i2_pe_dbanner_hanging_amount:{
       type: 'string'
     },
     fld_pfre_2i2_pe_dbanner_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2j_pe_raillies_quantity:{
       type: 'string'
     },
     fld_pfre_2j_pe_dbanner_mic_handmic_amount:{
       type: 'string'
     },
     fld_pfre_2j_pe_dbanner_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2k_pe_miking_vehicle_rent_amount:{
       type: 'string'
     },
     fld_pfre_2k_pe_miking_worker_amount:{
       type: 'string'
     },
     fld_pfre_2k_pe_miking_rent_amount:{
       type: 'string'
     },
     fld_pfre_2k_pe_miking_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2l_pe_potrait_quantity:{
       type: 'string'
     },
     fld_pfre_2l_pe_potrait_press_name:{
       type: 'string'
     },
     fld_pfre_2l_pe_potrait_prepare_amount:{
       type: 'string'
     },
     fld_pfre_2l_pe_potrait_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2m_pe_symbol_quantity:{
       type: 'string'
     },
     fld_pfre_2m_pe_symbol_prepare_amount:{
       type: 'string'
     },
     fld_pfre_2m_pe_symbol_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2n_pe_office_quantity:{
       type: 'string'
     },
     fld_pfre_2n_pe_office_daily_amount:{
       type: 'string'
     },
     fld_pfre_2n_pe_office_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2o_pe_worker_quantity:{
       type: 'string'
     },
     fld_pfre_2o_pe_worker_man_daily_amount:{
       type: 'string'
     },
     fld_pfre_2o_pe_worker_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2p_pe_tv_campaign_amount:{
       type: 'string'
     },
     fld_pfre_2p_pe_other_media_campaign_amount:{
       type: 'string'
     },
     fld_pfre_2p_pe_tv_other_media_total_expenditure_amount:{
       type: 'string'
     },

     fld_pfre_2q_pe_misc_scope_name:{
       type: 'string'
     },//misc = miscellaneous
     fld_pfre_2q_pe_misc_scope_amount:{
       type: 'string'
     },
     fld_pfre_2q_pe_tv_misc_scope_total_expenditure_amount:{
       type: 'string'
     },
     created_by:{
       type: 'string'
     },
		about:
		{
			type: 'string'
		},
		media_coverage:
		{
			type: 'json',
			defaultsTo: []
		},
		campaign_links:{
			type: 'json',
			defaultsTo: []
		},
		total_number_of_cases_present:{
			type: 'string'
		},
		total_number_of_cases_past:{
			type: 'string'
		},
		pfre_information_availability:{
			type: 'string'
		},
		affidavit_pdf:
		{
			type: 'string',
		},
		pfre_pdf:
		{
			type: 'string',
		},
		is_winner: {
			type: 'boolean',
			defaultsTo: false
		},
		vote_received: {
			type: 'integer',
			defaultsTo: 0
		},
		vote_percentage: {
			type: 'float',
			defaultsTo: 0
		},
   }
};
